package autoLogin.util;


import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auto.login.AutoLogin;
import com.liferay.portal.kernel.util.HttpUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Element;
import com.liferay.portal.kernel.xml.Namespace;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
// import com.liferay.portal.security.auth.AutoLogin;
// import com.liferay.portal.service.AssertionLocalServiceUtil;
// import com.liferay.portal.util.PortalUtil;

import saml.service.service.AssertionLocalServiceUtil;

public class SAML11ServiceProvider extends AbstractSAMLServiceProvider {
  @Override
  protected boolean receiveResponseInternal(HttpServletRequest request, String samlEntityId,
      String samlIdentityProviderId, String samlResponse, Date now, Document document)
      throws ParseException, SystemException {

    boolean authenticated = false;

    Element response = document.getRootElement();

    boolean statusSuccess = false;

    String statusCode = response.element(SAMLConstants.STATUS).element(SAMLConstants.STATUS_CODE)
        .attributeValue(SAMLConstants.VALUE);

    List<Namespace> samlps = response.getNamespacesForURI(SAMLConstants.PROTOCOL_NAMESPACE_V1);
    for (Namespace samlp : samlps) {
      String qualifiedName =
          SAXReaderUtil.createQName(SAMLConstants.SUCCESS, samlp).getQualifiedName();

      if (qualifiedName.equals(statusCode)) {
        statusSuccess = true;
      }
    }

    if (statusSuccess && SAMLUtil.validateXmlSignature(samlResponse, true)) {

      String majorVersion = response.attributeValue(SAMLConstants.MAJOR_VERSION);
      if (majorVersion == null) {
        _log.error("response without major version");
        return false;
      } else if (!majorVersion.equals("1")) {
        _log.error("major response version number not supported");
        return false;
      }

      String ACSUrl = SAMLMetadataManager.getSPAssertionConsumerServiceLocation(request);

      String recipient = response.attributeValue(SAMLConstants.RECIPIENT);
      if (recipient != null && !recipient.equals(ACSUrl)) {
        _log.error(
            "recipient must contains the service " + "provider's assertion consumer service URL");
        return false;
      }

      String inResponseTo = response.attributeValue(SAMLConstants.IN_RESPONSE_TO);
      if (inResponseTo != null) {
        _log.error("inResponseTo must not be present for " + "unsolicited response");
        return false;
      }

      Map<String, String> attributeValue = new HashMap<String, String>();
      request.setAttribute(SAMLConstants.ATTRIBUTE_VALUE_KEY, attributeValue);

      List<Element> assertions = response.elements(SAMLConstants.ASSERTION);
      assertions: for (Element assertion : assertions) {
        majorVersion = assertion.attributeValue(SAMLConstants.MAJOR_VERSION);
        if (majorVersion == null) {
          _log.error("assertion without major version");
          continue;
        } else if (!majorVersion.equals("1")) {
          _log.error("major assertion version number not supported");
          continue;
        }

        String issuer = assertion.attributeValue(SAMLConstants.ISSUER);
        if (issuer == null) {
          _log.error("assertion without issuer");
          continue;
        } else if (!issuer.equals(samlIdentityProviderId)) {
          _log.error("incorrect issuer in assertion");
          continue;
        }

        String assertionID = assertion.attributeValue(SAMLConstants.ASSERTION_ID);

        Element conditions = assertion.element(SAMLConstants.CONDITIONS);
        if (conditions != null) {
          Date notOnOrAfter =
              SAMLUtil.parseDate(conditions.attributeValue(SAMLConstants.NOT_ON_OR_AFTER));
          if (notOnOrAfter != null) {
            if (notOnOrAfter.after(now)) {
              if (AssertionLocalServiceUtil.replayedAssertion(assertionID, notOnOrAfter)) {
                _log.error("assertion replayed");
                continue;
              }
            } else {
              _log.error("Conditions NotOnOrAfter is passed");
              continue;
            }
          }

          List<Element> audienceRestrictions =
              conditions.elements(SAMLConstants.AUDIENCE_RESTRICTION_CONDITION);
          for (Element audienceRestriction : audienceRestrictions) {
            List<Element> audiences = audienceRestriction.elements(SAMLConstants.AUDIENCE);
            boolean notFind = true;
            for (Element audience : audiences) {
              if (audience.getText().equals(samlEntityId)) {
                notFind = false;
                break;
              }
            }
            if (notFind) {
              continue assertions;
            }
          }
        }

        String authenticationSubjectId = null;

        List<Element> authenticationStatements =
            assertion.elements(SAMLConstants.AUTHENTICATION_STATEMENT);
        for (Element authenticationStatement : authenticationStatements) {

          Element subject = authenticationStatement.element(SAMLConstants.SUBJECT);
          authenticationSubjectId = subject.element(SAMLConstants.NAME_IDENTIFIER).getText();

          if (authenticationSubjectId == null) {
            continue assertions;
          }

          Element subjectConfirmation = subject.element(SAMLConstants.SUBJECT_CONFIRMATION);
          if (!SAMLConstants.CONFIRMATION_METHOD_BEARER
              .equals(subjectConfirmation.element(SAMLConstants.CONFIRMATION_METHOD).getText())) {

            continue assertions;
          }

          authenticated = true;
        }

        List<Element> attributeStatements = assertion.elements(SAMLConstants.ATTRIBUTE_STATEMENT);
        for (Element attributeStatement : attributeStatements) {
          Element subject = attributeStatement.element(SAMLConstants.SUBJECT);

          String attributeSubjectId = subject.element(SAMLConstants.NAME_IDENTIFIER).getText();

          if (!authenticationSubjectId.equals(attributeSubjectId)) {
            continue assertions;
          }

          Element subjectConfirmation = subject.element(SAMLConstants.SUBJECT_CONFIRMATION);
          if (!SAMLConstants.CONFIRMATION_METHOD_BEARER
              .equals(subjectConfirmation.element(SAMLConstants.CONFIRMATION_METHOD).getText())) {

            continue assertions;
          }

          List<Element> attributes = attributeStatement.elements(SAMLConstants.ATTRIBUTE);
          for (Element attribute : attributes) {
            String name = attribute.attributeValue(SAMLConstants.ATTRIBUTE_NAME);

            if (_log.isDebugEnabled()) {
              _log.debug("name : " + name);
            }

            List<Element> attributeValues = attribute.elements(SAMLConstants.ATTRIBUTE_VALUE);
            if (!attributeValues.isEmpty()) {
              String value = attributeValues.get(0).getText();
              attributeValue.put(name, value);

              if (_log.isDebugEnabled()) {
                _log.debug("value : " + value);
              }
            }
          }
        }
      }
    }

    return authenticated;
  }

  @Override
  protected void sendAuthRequestInternal(HttpServletRequest request, String samlEntityId,
      String loginUrl, String target, String signAlg, Boolean isPassive) throws Exception {

    StringBundler url = new StringBundler(6);
    url.append(loginUrl).append(StringPool.QUESTION).append(SAMLConstants.TARGET)
        .append(StringPool.EQUAL).append(HttpUtil.encodeURL(PortalUtil.getPortalURL(request)))
        .append(HttpUtil.encodeURL(target));

    request.setAttribute(AutoLogin.AUTO_LOGIN_REDIRECT, url.toString());
  }



  private static Log _log = LogFactoryUtil.getLog(SAML11ServiceProvider.class);
}
