package autoLogin.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface SAMLServiceProvider {

  public boolean receiveResponse(HttpServletRequest request, String samlEntityId,
      String samlIdentityProviderId, String samlResponse) throws Exception;

  public void sendAuthnRequest(HttpServletRequest request, HttpServletResponse response,
      String samlEntityId, String loginUrl, String signAlg) throws Exception;

}
