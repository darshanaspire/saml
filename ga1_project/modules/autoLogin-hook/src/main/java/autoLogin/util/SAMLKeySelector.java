package autoLogin.util;

import java.security.cert.Certificate;

import javax.xml.crypto.AlgorithmMethod;
import javax.xml.crypto.KeySelector;
import javax.xml.crypto.KeySelectorException;
import javax.xml.crypto.KeySelectorResult;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;

/**
 * 
 * @author aspire101
 *
 */
class SAMLKeySelector extends KeySelector {

  public KeySelectorResult select(KeyInfo keyInfo, KeySelector.Purpose purpose,
      AlgorithmMethod method, XMLCryptoContext context) throws KeySelectorException {

    Certificate certificate = SAMLMetadataManager.getIDPSigningCertificate();

    if (certificate == null) {
      throw new KeySelectorException("IDP Certificate not found");
    }

    return new SimpleKeySelectorResult(certificate.getPublicKey());
  }

}
