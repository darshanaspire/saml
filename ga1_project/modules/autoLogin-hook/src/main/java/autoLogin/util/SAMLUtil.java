package autoLogin.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.security.Signature;
import java.security.cert.Certificate;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.DataFormatException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.XMLConstants;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.NodeList;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.struts.LastPath;
import com.liferay.portal.kernel.util.Base64;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HttpUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Element;
import com.liferay.portal.kernel.xml.Namespace;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

import jodd.io.StringInputStream;
import jodd.io.StringOutputStream;

public class SAMLUtil {
  public static String formatDate(Date date) {
    return _getDateFormat().format(date);
  }

  public static String generateAuthnRequest(HttpServletRequest request, String loginUrl,
      String samlEntityId, String relayState, String signAlg, Boolean isPassive) throws Exception {
    String messageID = generateMessageID();
    _log.info("generateAuthnRequest");
    request.getSession().setAttribute(SAMLConstants.MESSAGE_ID, messageID);

    Document document = SAXReaderUtil.createDocument();

    Namespace samlp =
        SAXReaderUtil.createNamespace(SAMLConstants.SAMLP, SAMLConstants.PROTOCOL_NAMESPACE);
    Namespace saml =
        SAXReaderUtil.createNamespace(SAMLConstants.SAML, SAMLConstants.ASSERTION_NAMESPACE);

    Element authnRequest =
        document.addElement(SAXReaderUtil.createQName(SAMLConstants.AUTHN_REQUEST, samlp));

    authnRequest.add(samlp);
    authnRequest.add(saml);

    authnRequest.addAttribute(SAMLConstants.ID, messageID);
    // authnRequest.addAttribute(SAMLConstants.ID, "");
    authnRequest.addAttribute(SAMLConstants.VERSION, SAMLConstants.VERSION_2_0);
    authnRequest.addAttribute(SAMLConstants.ISSUE_INSTANT, formatDate(new Date()));
    authnRequest.addAttribute(SAMLConstants.DESTINATION, loginUrl);

    if (isPassive != null) {
      authnRequest.addAttribute(SAMLConstants.IS_PASSIVE, isPassive.toString());
    }

    Element issuer = authnRequest.addElement(SAXReaderUtil.createQName(SAMLConstants.ISSUER, saml));
    _log.info("Before Set text");
    if (samlEntityId != null) {
      issuer.setText(samlEntityId);
    }

    _log.info("after Set text");
    String xmlString = document.asXML();
    if (_log.isDebugEnabled()) {
      _log.debug("send : " + xmlString);
    }

    return getHTTPRedirectBindingUrl(loginUrl, true, xmlString, relayState, signAlg);
  }

  public static String generateMessageID() {
    byte[] randomBytes = new byte[21];
    _random.nextBytes(randomBytes);
    return Base64.encode(randomBytes);
  }

  public static Map<String, String> getAttributes(HttpServletRequest request) {

    return (Map<String, String>) request.getAttribute(SAMLConstants.ATTRIBUTE_VALUE_KEY);
  }

  public static String getHTTPRedirectBindingUrl(String serviceUrl, boolean isRequest, String saml,
      String relayState, String signAlg)
      throws SystemException, IOException, GeneralSecurityException {
    _log.info("After getHTTPRedirectBindingUrl");
    byte[] samlDeflate = Deflate.encode(saml);

    String samlBase64 = Base64.encode(samlDeflate);

    StringBundler url = new StringBundler(7);
    url.append(serviceUrl).append(StringPool.QUESTION);

    StringBundler urlParamBuffer = new StringBundler(11);
    if (isRequest) {
      urlParamBuffer.append(SAMLConstants.SAML_REQUEST);
    } else {
      urlParamBuffer.append(SAMLConstants.SAML_RESPONSE);
    }
    urlParamBuffer.append(StringPool.EQUAL).append(HttpUtil.encodeURL(samlBase64));

    if (relayState != null) {
      urlParamBuffer.append(StringPool.AMPERSAND).append(SAMLConstants.RELAY_STATE)
          .append(StringPool.EQUAL).append(HttpUtil.encodeURL(relayState));
    }

    urlParamBuffer.append(StringPool.AMPERSAND).append(SAMLConstants.SIG_ALG)
        .append(StringPool.EQUAL)
        .append(URLEncoder.encode(_getURIFromSignAlg(signAlg), StringPool.UTF8));

    String urlParameters = urlParamBuffer.toString();

    url.append(urlParameters);
    _log.info(" Before Sign url ---> " + url);
    url.append(StringPool.AMPERSAND).append(SAMLConstants.SIGNATURE).append(StringPool.EQUAL)
        .append(_signUrlParameter(urlParameters, signAlg));

    _log.info(" After Sign url ---> " + url);

    return url.toString();
  }

  public static String getRelayStateFromLastPath(HttpServletRequest request) {

    String relayState = null;
    if (PropsValues.AUTH_FORWARD_BY_LAST_PATH) {
      LastPath lastPath = (LastPath) request.getSession().getAttribute(WebKeys.LAST_PATH);
      if (lastPath != null) {
        // relayState = lastPath.getContextPath().concat(lastPath.getPath());
        relayState = "http://localhost:8000";
        _log.info("RelayState AUTH_FORWARD_BY_LAST_PATH" + relayState);
      }
    } else {
      relayState = ParamUtil.getString(request, "redirect");
      _log.info("RelayState --> " + relayState);
    }
    return relayState;
  }

  public static String getSamlFromHTTPPostBindingParameter(String parameter)
      throws UnsupportedEncodingException {

    String saml = new String(Base64.decode(parameter), StringPool.UTF8);
    _log.info("-----------------------------------------------------------------------");
    _log.info(" Base 64 Decoded : " + saml);
    _log.info("-----------------------------------------------------------------------");

    if (_log.isDebugEnabled()) {
      _log.debug("receive : " + saml);
    }
    return saml;
  }

  public static String getSamlFromHTTPRedirectBindingParameter(String parameter)
      throws DataFormatException, UnsupportedEncodingException {

    byte[] samlDeflate = Base64.decode(parameter);
    _log.info("-----------------------------------------------------------------------");
    _log.info(" Base 64 Decoded : " + samlDeflate.toString());
    _log.info("-----------------------------------------------------------------------");
    String saml = Deflate.decode(samlDeflate);

    if (_log.isDebugEnabled()) {
      _log.debug("receive : " + saml);
    }
    return saml;
  }

  public static void initHTTPHeader(HttpServletResponse response) {
    response.setHeader("Cache-Control", "no-cache, no-store");
    response.setHeader("Pragma", "no-cache");
  }

  public static boolean isAuthenticated(HttpServletRequest request, String loginUrl,
      String samlEntityId, String samlIdentityProviderId, String signAlg,
      HttpServletResponse response) throws Exception {
    _log.info("This is isAuthenticated - -----");
    SAMLServiceProvider serviceProvider = SAMLMetadataManager.getServiceProvider();

    String samlResponseParameter = request.getParameter(SAMLConstants.SAML_RESPONSE);
    _log.info("samlResponseParameter --> " + samlResponseParameter);
    if (samlResponseParameter != null) {
      _log.info("This is Inside SamlResponseParameter");
      String samlResponse = getSamlFromHTTPPostBindingParameter(samlResponseParameter);

      return serviceProvider.receiveResponse(request, samlEntityId, samlIdentityProviderId,
          samlResponse);
    } else {
      _log.info("ELSE Inside SamlResponseParameter");
      serviceProvider.sendAuthnRequest(request, response, samlEntityId, loginUrl, signAlg);
    }

    return false;
  }

  public static Date parseDate(String source) throws ParseException {
    int index = source.indexOf(CharPool.PERIOD);
    if (index != -1) {
      source = source.substring(0, index).concat("Z");
    }
    return _getDateFormat().parse(source);
  }

  public static boolean validateUrlParameterSignature(HttpServletRequest request) throws Exception {

    // String uri = GetterUtil.getString(request.getParameter(SAMLConstants.SIG_ALG));
    String uri = "";
    String urlSignature = GetterUtil.getString(request.getParameter(SAMLConstants.SIGNATURE));

    String urlParameters = GetterUtil.getString(request.getQueryString());

    // reconstructs the url's parameter correctly ordered
    // (let the url encoding)

    String saml = null;
    boolean isRequest = false;
    String relayState = null;
    String sigAlg = null;

    String[] split = urlParameters.split("[&=]");
    for (int i = 0; i < split.length; ++i) {
      String name = split[i];
      String value = split[i];
      if (SAMLConstants.SAML_REQUEST.equals(name)) {
        saml = value;
        isRequest = true;
      } else if (SAMLConstants.SAML_RESPONSE.equals(name)) {
        saml = value;
      } else if (SAMLConstants.RELAY_STATE.equals(name)) {
        relayState = value;
      } else if (SAMLConstants.SIG_ALG.equals(name)) {
        sigAlg = value;
      }
    }

    StringBundler urlParamBuffer = new StringBundler(11);
    if (isRequest) {
      urlParamBuffer.append(SAMLConstants.SAML_REQUEST);
    } else {
      urlParamBuffer.append(SAMLConstants.SAML_RESPONSE);
    }
    urlParamBuffer.append(StringPool.EQUAL).append(saml);

    if (relayState != null) {
      urlParamBuffer.append(StringPool.AMPERSAND).append(SAMLConstants.RELAY_STATE)
          .append(StringPool.EQUAL).append(relayState);
    }

    urlParamBuffer.append(StringPool.AMPERSAND).append(SAMLConstants.SIG_ALG)
        .append(StringPool.EQUAL).append(sigAlg);

    Certificate certificate = SAMLMetadataManager.getIDPSigningCertificate();

    Signature signature = Signature.getInstance(_getAlgorithmFromURI(uri));
    signature.initVerify(certificate);

    urlParameters = urlParamBuffer.toString();

    signature.update(urlParameters.getBytes(StringPool.UTF8));

    return signature.verify(Base64.decode(urlSignature));
  }

  public static boolean validateXmlSignature(String xml, boolean saml11) {
    // if(XmlValidator.validateXml(xml)){
    //
    // }


    // xml =
    // "<samlp:Response xmlns:samlp='urn:oasis:names:tc:SAML:2.0:protocol'
    // xmlns:saml='urn:oasis:names:tc:SAML:2.0:assertion'
    // ID='_ac753422d9f1fed6b56e4155bb5a6db3bd5526105e' Version='2.0'
    // IssueInstant='2019-10-11T12:27:37Z' Destination='http://localhost:8000/group/guest/payment'
    // InResponseTo='bFd+y7GYQVJt5WdsDEwX5EjNJFBn'><saml:Issuer>http://localhost:8080/simplesaml/saml2/idp/metadata.php</saml:Issuer><ds:Signature
    // xmlns:ds='http://www.w3.org/2000/09/xmldsig#'><ds:SignedInfo><ds:CanonicalizationMethod
    // Algorithm='http://www.w3.org/2001/10/xml-exc-c14n#'/><ds:SignatureMethod
    // Algorithm='http://www.w3.org/2000/09/xmldsig#rsa-sha1'/><ds:Reference
    // URI='#_ac753422d9f1fed6b56e4155bb5a6db3bd5526105e'><ds:Transforms><ds:Transform
    // Algorithm='http://www.w3.org/2000/09/xmldsig#enveloped-signature'/><ds:Transform
    // Algorithm='http://www.w3.org/2001/10/xml-exc-c14n#'/></ds:Transforms><ds:DigestMethod
    // Algorithm='http://www.w3.org/2000/09/xmldsig#sha1'/><ds:DigestValue>3g+aaby1rlXVxBuu54RBqd2qz80=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue>a2u5YtFXbe+6n9CDbzBpqIQ2OwQgH0+eXKHsEXId9GTgeIM+i9mPy8pm6BeH1NONlRu7dNBVW41fdbaiOV3kO9UrETbJAM+hXexgx/nt8EClMEUKrHkzH55RSOIyb2axwnYjODXr747AsLOz5lTgOxhyzRqg69a21hhT6SQBaFPnwTj2JcDDgct5jL8u1h86BYfwd6DVh0yoOzzuyiZ5brNn9/+YZhGIi/1/4wO3Y4XFhnNACY/b1kzazgCP0RBfqjNQyEX3R29Kb2SBXTNEnvAEGCAkgwSTLTdQOfE4VQfKOVzebiF7PXXduF0ophA8xGhyvqzom9E94iQpuI6Q8A==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIIDXTCCAkWgAwIBAgIJALmVVuDWu4NYMA0GCSqGSIb3DQEBCwUAMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEwHwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwHhcNMTYxMjMxMTQzNDQ3WhcNNDgwNjI1MTQzNDQ3WjBFMQswCQYDVQQGEwJBVTETMBEGA1UECAwKU29tZS1TdGF0ZTEhMB8GA1UECgwYSW50ZXJuZXQgV2lkZ2l0cyBQdHkgTHRkMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzUCFozgNb1h1M0jzNRSCjhOBnR+uVbVpaWfXYIR+AhWDdEe5ryY+CgavOg8bfLybyzFdehlYdDRgkedEB/GjG8aJw06l0qF4jDOAw0kEygWCu2mcH7XOxRt+YAH3TVHa/Hu1W3WjzkobqqqLQ8gkKWWM27fOgAZ6GieaJBN6VBSMMcPey3HWLBmc+TYJmv1dbaO2jHhKh8pfKw0W12VM8P1PIO8gv4Phu/uuJYieBWKixBEyy0lHjyixYFCR12xdh4CA47q958ZRGnnDUGFVE1QhgRacJCOZ9bd5t9mr8KLaVBYTCJo5ERE8jymab5dPqe5qKfJsCZiqWglbjUo9twIDAQABo1AwTjAdBgNVHQ4EFgQUxpuwcs/CYQOyui+r1G+3KxBNhxkwHwYDVR0jBBgwFoAUxpuwcs/CYQOyui+r1G+3KxBNhxkwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEAAiWUKs/2x/viNCKi3Y6blEuCtAGhzOOZ9EjrvJ8+COH3Rag3tVBWrcBZ3/uhhPq5gy9lqw4OkvEws99/5jFsX1FJ6MKBgqfuy7yh5s1YfM0ANHYczMmYpZeAcQf2CGAaVfwTTfSlzNLsF2lW/ly7yapFzlYSJLGoVE+OHEu8g5SlNACUEfkXw+5Eghh+KzlIN7R6Q7r2ixWNFBC/jWf7NKUfJyX8qIG5md1YUeT6GBW9Bm2/1/RiO24JTaYlfLdKK9TYb8sG5B+OLab2DImG99CJ25RkAcSobWNF5zD0O6lgOo3cEdB/ksCq3hmtlC/DlLZ/D8CJ+7VuZnS1rR2naQ==</ds:X509Certificate></ds:X509Data></ds:KeyInfo></ds:Signature><samlp:Status><samlp:StatusCode
    // Value='urn:oasis:names:tc:SAML:2.0:status:Success'/></samlp:Status><saml:Assertion
    // xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'
    // xmlns:xs='http://www.w3.org/2001/XMLSchema' ID='_10236c885993f425385f760081415f9729e08c153f'
    // Version='2.0'
    // IssueInstant='2019-10-11T12:27:37Z'><saml:Issuer>http://localhost:8080/simplesaml/saml2/idp/metadata.php</saml:Issuer><ds:Signature
    // xmlns:ds='http://www.w3.org/2000/09/xmldsig#'><ds:SignedInfo><ds:CanonicalizationMethod
    // Algorithm='http://www.w3.org/2001/10/xml-exc-c14n#'/><ds:SignatureMethod
    // Algorithm='http://www.w3.org/2000/09/xmldsig#rsa-sha1'/><ds:Reference
    // URI='#_10236c885993f425385f760081415f9729e08c153f'><ds:Transforms><ds:Transform
    // Algorithm='http://www.w3.org/2000/09/xmldsig#enveloped-signature'/><ds:Transform
    // Algorithm='http://www.w3.org/2001/10/xml-exc-c14n#'/></ds:Transforms><ds:DigestMethod
    // Algorithm='http://www.w3.org/2000/09/xmldsig#sha1'/><ds:DigestValue>d7ph4Kc5y/Ul6U4Mtt1p8SgX3ZU=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue>Tbb0PulZcTmPjbe35SycUtZ0oVoNbnqNhUQxZCedYkwSrmXSnRq6CYSm8uRykCWNWvLTqf3YowsiMOe1mYkw6yzWHfXyksfS1I9tF5yR5ntpngUTbizbzD6VhMicVh4hMoVXMQ57+YPusG8mKASuUbA2MMEcZL+wHyxaJRBl6YOWC85iS82YHG1BVwGiNuQnSjBRg4nt/bMuvZnUyeaIk6tJwPjnn1FK5QY4aUhv2rgI5CQEanxvJJidbUSh26sxyjO5+PR/TboTJQPRsB7IcUm6BB7tDMuuQNogM116GdoDoLChbOtLe3XlqXCmZDvd4dNxrdCgxTHb1bSA3KkAHg==</ds:SignatureValue><ds:KeyInfo><ds:X509Data><ds:X509Certificate>MIIDXTCCAkWgAwIBAgIJALmVVuDWu4NYMA0GCSqGSIb3DQEBCwUAMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEwHwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwHhcNMTYxMjMxMTQzNDQ3WhcNNDgwNjI1MTQzNDQ3WjBFMQswCQYDVQQGEwJBVTETMBEGA1UECAwKU29tZS1TdGF0ZTEhMB8GA1UECgwYSW50ZXJuZXQgV2lkZ2l0cyBQdHkgTHRkMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzUCFozgNb1h1M0jzNRSCjhOBnR+uVbVpaWfXYIR+AhWDdEe5ryY+CgavOg8bfLybyzFdehlYdDRgkedEB/GjG8aJw06l0qF4jDOAw0kEygWCu2mcH7XOxRt+YAH3TVHa/Hu1W3WjzkobqqqLQ8gkKWWM27fOgAZ6GieaJBN6VBSMMcPey3HWLBmc+TYJmv1dbaO2jHhKh8pfKw0W12VM8P1PIO8gv4Phu/uuJYieBWKixBEyy0lHjyixYFCR12xdh4CA47q958ZRGnnDUGFVE1QhgRacJCOZ9bd5t9mr8KLaVBYTCJo5ERE8jymab5dPqe5qKfJsCZiqWglbjUo9twIDAQABo1AwTjAdBgNVHQ4EFgQUxpuwcs/CYQOyui+r1G+3KxBNhxkwHwYDVR0jBBgwFoAUxpuwcs/CYQOyui+r1G+3KxBNhxkwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEAAiWUKs/2x/viNCKi3Y6blEuCtAGhzOOZ9EjrvJ8+COH3Rag3tVBWrcBZ3/uhhPq5gy9lqw4OkvEws99/5jFsX1FJ6MKBgqfuy7yh5s1YfM0ANHYczMmYpZeAcQf2CGAaVfwTTfSlzNLsF2lW/ly7yapFzlYSJLGoVE+OHEu8g5SlNACUEfkXw+5Eghh+KzlIN7R6Q7r2ixWNFBC/jWf7NKUfJyX8qIG5md1YUeT6GBW9Bm2/1/RiO24JTaYlfLdKK9TYb8sG5B+OLab2DImG99CJ25RkAcSobWNF5zD0O6lgOo3cEdB/ksCq3hmtlC/DlLZ/D8CJ+7VuZnS1rR2naQ==</ds:X509Certificate></ds:X509Data></ds:KeyInfo></ds:Signature><saml:Subject><saml:NameID
    // SPNameQualifier='saml-poc'
    // Format='urn:oasis:names:tc:SAML:2.0:nameid-format:transient'>_26f558f718af9d1be989e5a9d76224425a297676b8</saml:NameID><saml:SubjectConfirmation
    // Method='urn:oasis:names:tc:SAML:2.0:cm:bearer'><saml:SubjectConfirmationData
    // NotOnOrAfter='2019-10-11T12:32:37Z' Recipient='http://localhost:8000/group/guest/payment'
    // InResponseTo='bFd+y7GYQVJt5WdsDEwX5EjNJFBn'/></saml:SubjectConfirmation></saml:Subject><saml:Conditions
    // NotBefore='2019-10-11T12:27:07Z'
    // NotOnOrAfter='2019-10-11T12:32:37Z'><saml:AudienceRestriction><saml:Audience>saml-poc</saml:Audience></saml:AudienceRestriction></saml:Conditions><saml:AuthnStatement
    // AuthnInstant='2019-10-11T09:33:18Z' SessionNotOnOrAfter='2019-10-11T17:33:18Z'
    // SessionIndex='_92e1ea1cd30345a04eebb8e03dde3a2a8df326aed5'><saml:AuthnContext><saml:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:Password</saml:AuthnContextClassRef></saml:AuthnContext></saml:AuthnStatement><saml:AttributeStatement><saml:Attribute
    // Name='uid'
    // NameFormat='urn:oasis:names:tc:SAML:2.0:attrname-format:basic'><saml:AttributeValue
    // xsi:type='xs:string'>1</saml:AttributeValue></saml:Attribute><saml:Attribute
    // Name='eduPersonAffiliation'
    // NameFormat='urn:oasis:names:tc:SAML:2.0:attrname-format:basic'><saml:AttributeValue
    // xsi:type='xs:string'>group1</saml:AttributeValue></saml:Attribute><saml:Attribute
    // Name='email'
    // NameFormat='urn:oasis:names:tc:SAML:2.0:attrname-format:basic'><saml:AttributeValue
    // xsi:type='xs:string'>user1@example.com</saml:AttributeValue></saml:Attribute></saml:AttributeStatement></saml:Assertion></samlp:Response>";
    xml = removeDoubleWhiteSpaces(xml);
    _log.info("XML ---->  " + xml);


    try {

      // Instantiate the document to be validated

      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      dbf.setNamespaceAware(true);
      org.w3c.dom.Document doc =
          dbf.newDocumentBuilder().parse(new ByteArrayInputStream(xml.getBytes(StringPool.UTF8)));

      if (saml11) {

        // for SAML 1.1, we need to declare the attribute
        // to be an id attribute

        org.w3c.dom.Element rootElement = doc.getDocumentElement();
        rootElement.setIdAttribute(SAMLConstants.RESPONSE_ID, true);
      }

      // Find Signature element
      NodeList nl = doc.getElementsByTagNameNS(XMLSignature.XMLNS, SAMLConstants.SIGNATURE);

      if (nl.getLength() == 0) {
        _log.error("Cannot find Signature element");
        return false;
      }

      // Create a DOM XMLSignatureFactory that will be used to unmarshal
      // the document containing the XMLSignature

      XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");
      _log.info("nl.getLength() -> " + nl.getLength());
      _log.info("item(0)  -> " + nl.item(0).getNodeValue());
      _log.info("item(1) -> " + nl.item(1).getNodeValue());
      for (int i = 0; i < nl.getLength(); ++i) {

        // Create a DOMValidateContext and specify a KeyValue
        // KeySelector and document context
        _log.info("nl.item(" + i + ").getNodeValue() -> " + nl.item(i).getNodeValue());
        // if (nl.item(i).getNodeValue() != null) {

        DOMValidateContext valContext = new DOMValidateContext(new SAMLKeySelector(), nl.item(i));
        _log.info("nl.item(i) -> " + nl.item(i));
        // unmarshal the XMLSignature

        XMLSignature signature = fac.unmarshalXMLSignature(valContext);
        _log.info("valContext -> " + valContext.getNode());
        // Validate the XMLSignature

        if (signature.validate(valContext)) {
          _log.info("signature.validate TRUE");
          // if (_log.isDebugEnabled()) {
          // _log.debug("Signature " + i + " passed core validation");
          // }
        } else {
          _log.info("signature.validate FALSE");
          _log.error("Signature " + i + " failed core validation");

          if (_log.isInfoEnabled()) {
            boolean sv = signature.getSignatureValue().validate(valContext);
            _log.info("signature validation status: " + sv);

            // check the validation status of each Reference

            Iterator<Reference> itRef = signature.getSignedInfo().getReferences().iterator();
            for (int j = 0; itRef.hasNext(); j++) {
              boolean refValid = itRef.next().validate(valContext);
              _log.info("ref[" + j + "] validity status: " + refValid);
            }
          }
          _log.info("validateXmlSignature : FALSE");
          return false;
        }
      }
      // }
    } catch (Exception e) {
      _log.info("validateXmlSignature : FALSE");
      _log.error(e, e);
      return false;
    }
    _log.info("validateXmlSignature : TRUE");
    return true;
  }

  public static String removeDoubleWhiteSpaces(String s) {

    // Creating a pattern for whitespaces
    Pattern patt = Pattern.compile("\\s\\s");

    // Searching patt in s.
    Matcher mat = patt.matcher(s);

    // Replacing
    mat.replaceAll("");

    return mat.replaceAll("");
  }

  // private static String _getAlgorithmFromSignAlg(String signAlg) {
  // if (_SHA256_WITH_RSA.equals(signAlg)) {
  // return _SHA256_WITH_RSA;
  // }
  // return _SHA1_WITH_RSA;
  // }
  //
  // private static String _getAlgorithmFromURI(String uri) {
  // if (SignatureMethod.DSA_SHA1.equals(uri)) {
  // return _SHA1_WITH_DSA;
  // }
  // return _SHA1_WITH_RSA;
  // }
  //
  // private static DateFormat _getDateFormat() {
  // return new SimpleDateFormat(_DATE_PATTERN);
  // }
  //
  // private static String _getURIFromSignAlg(String signAlg) {
  // if (_SHA256_WITH_RSA.equals(signAlg)) {
  // return "http://www.w3.org/2000/09/xmldsig#rsa-sha256";
  // }
  // return SignatureMethod.RSA_SHA1;
  // }
  private static String _getAlgorithmFromSignAlg(String signAlg) {
    if (_DSA_WITH_SHA1.equals(signAlg)) {
      return _SHA1_WITH_DSA;
    }
    return _SHA1_WITH_RSA;
  }

  private static String _getAlgorithmFromURI(String uri) {
    if (SignatureMethod.DSA_SHA1.equals(uri)) {
      return _SHA1_WITH_DSA;
    }
    return _SHA1_WITH_RSA;
  }

  private static DateFormat _getDateFormat() {
    return new SimpleDateFormat(_DATE_PATTERN);
  }

  private static String _getURIFromSignAlg(String signAlg) {
    if (_DSA_WITH_SHA1.equals(signAlg)) {
      return SignatureMethod.DSA_SHA1;
    }
    return SignatureMethod.RSA_SHA1;
  }

  private static String _signUrlParameter(String urlParameter, String signAlg)
      throws SystemException, IOException, GeneralSecurityException {

    Signature signature = Signature.getInstance(_getAlgorithmFromSignAlg(signAlg));
    signature.initSign(SAMLMetadataManager.getSPPrivateKey());

    signature.update(urlParameter.getBytes(StringPool.UTF8));
    return HttpUtil.encodeURL(Base64.encode(signature.sign()));
  }

  private static String _signXml(String xml, String signedId) throws Exception {

    // Create a DOM XMLSignatureFactory that will be used to generate the
    // enveloped signature

    XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

    // Create a Reference to the enveloped document (in this case we are
    // signing the whole document, so a URI of "" signifies that) and
    // also specify the SHA1 digest algorithm and the ENVELOPED Transform.

    List<Transform> transforms = new ArrayList<Transform>(2);
    transforms.add(fac.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null));
    transforms.add(fac.newTransform(CanonicalizationMethod.EXCLUSIVE_WITH_COMMENTS,
        (TransformParameterSpec) null));

    Reference ref = fac.newReference(StringPool.POUND.concat(signedId),
        fac.newDigestMethod(DigestMethod.SHA1, null), transforms, null, null);

    // Create the SignedInfo

    SignedInfo si = fac.newSignedInfo(
        fac.newCanonicalizationMethod(CanonicalizationMethod.EXCLUSIVE_WITH_COMMENTS,
            (C14NMethodParameterSpec) null),
        fac.newSignatureMethod(SignatureMethod.RSA_SHA1, null), Collections.singletonList(ref));

    KeyInfoFactory kif = fac.getKeyInfoFactory();
    X509Data x509Data =
        kif.newX509Data(Collections.singletonList(SAMLMetadataManager.getSPCertificate()));

    // Create a KeyInfo and add the X509Data to it

    KeyInfo ki = kif.newKeyInfo(Collections.singletonList(x509Data));

    // Instantiate the document to be signed

    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    dbf.setNamespaceAware(true);
    org.w3c.dom.Document doc = dbf.newDocumentBuilder().parse(new StringInputStream(xml));

    // Create a DOMSignContext and specify the DSA PrivateKey and
    // location of the resulting XMLSignature's parent element

    DOMSignContext dsc =
        new DOMSignContext(SAMLMetadataManager.getSPPrivateKey(), doc.getDocumentElement());

    // Create the XMLSignature (but don't sign it yet)

    XMLSignature signature = fac.newXMLSignature(si, ki);

    // Marshal, generate (and sign) the enveloped signature

    signature.sign(dsc);

    // output the resulting document

    StringOutputStream os = new StringOutputStream();

    TransformerFactory tf = TransformerFactory.newInstance();
    Transformer trans = tf.newTransformer();
    trans.transform(new DOMSource(doc), new StreamResult(os));

    return os.toString();
  }

  private static final String _DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'Z'";

  private static final String _DSA_WITH_SHA1 = "DSAwithSHA1";

  private static final String _SHA1_WITH_DSA = "SHA1withDSA";
  private static final String _SHA256_WITH_RSA = "SHA256withRSA";

  private static final String _SHA1_WITH_RSA = "SHA1withRSA";

  private static Log _log = LogFactoryUtil.getLog(SAMLUtil.class);

  private static Random _random = new Random();
}
