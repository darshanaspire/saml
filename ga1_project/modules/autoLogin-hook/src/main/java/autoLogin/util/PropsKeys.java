package autoLogin.util;

public class PropsKeys {
  public static final String RSS_CONNECTION_TIMEOUT = "rss.connection.timeout";

  public static final String SAML_ADDRESS_CITY_ATTRS = "saml.address.city.attrs";

  public static final String SAML_ADDRESS_COUNTRY_ATTRS = "saml.address.country.attrs";

  public static final String SAML_ADDRESS_DEFAULT_COUNTRY_ID = "saml.address.default.country.id";

  public static final String SAML_ADDRESS_POSTALCODE_ATTRS = "saml.address.postalcode.attrs";

  public static final String SAML_ADDRESS_REGION_ATTRS = "saml.address.region.attrs";

  public static final String SAML_ADDRESS_STREET_ATTRS = "saml.address.street.attrs";

  public static final String SAML_ADDRESS_TYPES_ID = "saml.address.types.id";

  public static final String SAML_AUTH_ENABLED = "saml.auth.enabled";

  public static final String SAML_BIRTH_DAY_ATTR = "saml.birth.day.attr";

  public static final String SAML_BIRTH_DAY_FORMAT = "saml.birth.day.format";

  public static final String SAML_EMAIL_ADDRESS_ATTR = "saml.email.address.attr";

  public static final String SAML_ENTITY_ID = "saml.entity.id";

  public static final String SAML_EXCLUDED_AGENT = "saml.excluded.agent";

  public static final String SAML_EXCLUDED_URL = "saml.excluded.url";

  public static final String SAML_FIRST_NAME_ATTR = "saml.first.name.attr";

  public static final String SAML_IDENTITY_PROVIDER_ID = "saml.identity.provider.id";

  public static final String SAML_IDENTITY_PROVIDER_METADATA = "saml.identy.provider.metadata";

  public static final String SAML_INTER_SITE_TRANSFER_SERVICE_URL =
      "saml.intersite.transfer.service.url";

  public static final String SAML_KEYSTORE_ALIAS = "saml.keystore.alias";

  public static final String SAML_KEYSTORE_FILE = "saml.keystore.file";

  public static final String SAML_KEYSTORE_IDP_ALIAS = "saml.keystore.idp.alias";

  public static final String SAML_KEYSTORE_KEY_PASS = "saml.keystore.key.pass";

  public static final String SAML_KEYSTORE_PASS = "saml.keystore.pass";

  public static final String SAML_KEYSTORE_TYPE = "saml.keystore.type";

  public static final String SAML_LAST_NAME_ATTR = "saml.last.name.attr";

  public static final String SAML_LDAP_IMPORT_ENABLED = "saml.ldap.import.enabled";

  public static final String SAML_LOGIN_URL = "saml.login.url";

  public static final String SAML_PHONE_ATTRS = "saml.phone.attrs";

  public static final String SAML_PHONE_TYPES_ID = "saml.phone.types.id";

  public static final String SAML_REDIRECT_SIGN_ALG = "saml.redirect.sign.alg";

  public static final String SAML_SCREEN_NAME_ATTR = "saml.screen.name.attr";

  public static final String SAML_VERSION = "saml.version";

  public static final String SANDBOX_DEPLOY_DIR = "sandbox.deploy.dir";

  public static final String SANDBOX_DEPLOY_ENABLED = "sandbox.deploy.enabled";

  public static final String AUTH_FORWARD_BY_LAST_PATH = "auth.forward.by.last.path";

  public static final String INVALIDATE_SESSION_FROM_IDP = "INVALIDATE_SESSION_FROM_IDP";

  public static final String LAST_PATH = "LAST_PATH";

  public static final String LAYOUT = "LAYOUT";
}
