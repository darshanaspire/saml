package autoLogin.util;

public class SAMLConstants {


  public static final String ASSERTION = "Assertion";

  public static final String ASSERTION_CONSUMER_SERVICE = "AssertionConsumerService";

  // public static final String ASSERTION_CONSUMER_SERVICE_URL = "/saml/assertion_consumer_service";
  public static final String ASSERTION_CONSUMER_SERVICE_URL = "/group/guest/payment";

  public static final String ASSERTION_ID = "AssertionID";

  public static final String ASSERTION_NAMESPACE = "urn:oasis:names:tc:SAML:2.0:assertion";

  public static final String ATTRIBUTE = "Attribute";

  public static final String ATTRIBUTE_NAME = "AttributeName";

  public static final String ATTRIBUTE_STATEMENT = "AttributeStatement";

  public static final String ATTRIBUTE_VALUE = "AttributeValue";

  public static final String ATTRIBUTE_VALUE_KEY = "saml.attribute.value";

  public static final String AUDIENCE = "Audience";

  public static final String AUDIENCE_RESTRICTION = "AudienceRestriction";

  public static final String AUDIENCE_RESTRICTION_CONDITION = "AudienceRestrictionCondition";

  public static final String AUTHENTICATION_STATEMENT = "AuthenticationStatement";

  public static final String AUTHN_REQUEST = "AuthnRequest";

  public static final String AUTHN_REQUESTS_SIGNED = "AuthnRequestsSigned";

  public static final String AUTHN_STATEMENT = "AuthnStatement";

  public static final String BINDING = "Binding";

  public static final String CONDITIONS = "Conditions";

  public static final String CONFIRMATION_METHOD = "ConfirmationMethod";

  public static final String CONFIRMATION_METHOD_BEARER = "urn:oasis:names:tc:SAML:1.0:cm:bearer";

  public static final String DESTINATION = "Destination";

  public static final String DS = "ds";

  public static final String ENCRYPTED_ID = "EncryptedID";

  public static final String ENCRYPTED_ID_KEY = "saml.encrypted.id";

  public static final String ENCRYPTION = "encryption";

  public static final String ENTITY_DESCRIPTOR = "EntityDescriptor";

  public static final String ENTITY_ID = "entityID";

  public static final String HTTP_POST = "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST";

  public static final String HTTP_REDIRECT = "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect";

  public static final String ID = "ID";

  public static final String IDP_INTERROGATED = "saml.idp.interrogated";

  public static final String IDP_SSO_DESCRIPTOR = "IDPSSODescriptor";

  public static final String IN_RESPONSE_TO = "InResponseTo";

  public static final String INDEX = "index";

  public static final String IS_DEFAULT = "isDefault";

  public static final String IS_PASSIVE = "IsPassive";

  public static final String ISSUE_INSTANT = "IssueInstant";

  public static final String ISSUER = "Issuer";

  public static final String KEY_DESCRIPTOR = "KeyDescriptor";

  public static final String KEY_INFO = "KeyInfo";

  public static final String LOCATION = "Location";

  public static final String LOGOUT_REQUEST = "LogoutRequest";

  public static final String LOGOUT_RESPONSE = "LogoutResponse";

  public static final String MAJOR_VERSION = "MajorVersion";

  public static final String MD = "md";

  public static final String MESSAGE_ID = "saml.message.id";

  public static final String METADATA_NAMESPACE = "urn:oasis:names:tc:SAML:2.0:metadata";

  public static final String METHOD = "Method";

  public static final String NAME = "Name";

  public static final String NAME_ID = "NameID";

  public static final String NAME_ID_FORMAT = "NameIDFormat";

  public static final String NAME_ID_KEY = "saml.name.id";

  public static final String NAME_ID_TRANSIENT_FORMAT =
      "urn:oasis:names:tc:SAML:2.0:nameid-format:transient";

  public static final String NAME_IDENTIFIER = "NameIdentifier";

  public static final String NOT_ON_OR_AFTER = "NotOnOrAfter";

  public static final String PROTOCOL_NAMESPACE = "urn:oasis:names:tc:SAML:2.0:protocol";

  public static final String PROTOCOL_NAMESPACE_V1 = "urn:oasis:names:tc:SAML:1.0:protocol";

  public static final String PROTOCOL_SUPPORT_ENUMERATION = "protocolSupportEnumeration";

  public static final String RECIPIENT = "Recipient";

  public static final String RELAY_STATE = "RelayState";

  public static final String RESPONSE_ID = "ResponseID";

  public static final String RESPONSE_LOCATION = "ResponseLocation";

  public static final String SAML = "saml";

  public static final String SAML_REQUEST = "SAMLRequest";

  public static final String SAML_RESPONSE = "SAMLResponse";

  public static final String SAMLP = "samlp";

  public static final String SESSION_INDEX = "SessionIndex";

  public static final String SESSION_INDEX_KEY = "saml.session.index";

  public static final String SESSION_NOT_AFTER = "saml.session.not.after";

  public static final String SESSION_NOT_ON_OR_AFTER = "SessionNotOnOrAfter";

  public static final String SIG_ALG = "SigAlg";

  public static final String SIGNATURE = "Signature";

  public static final String SIGNING = "signing";

  public static final String SINGLE_LOGOUT_SERVICE = "SingleLogoutService";

  public static final String SINGLE_LOGOUT_URL = "/saml/single_logout";

  public static final String SINGLE_SIGN_ON_SERVICE = "SingleSignOnService";

  public static final String SP_SSO_DESCRIPTOR = "SPSSODescriptor";

  public static final String STATUS = "Status";

  public static final String STATUS_CODE = "StatusCode";

  public static final String SUBJECT = "Subject";

  public static final String SUBJECT_CONFIRMATION = "SubjectConfirmation";

  public static final String SUBJECT_CONFIRMATION_DATA = "SubjectConfirmationData";

  public static final String SUBJECT_CONFIRMATION_METHOD_BEARER =
      "urn:oasis:names:tc:SAML:2.0:cm:bearer";

  public static final String SUCCESS = "Success";

  public static final String TARGET = "TARGET";

  public static final String URN_STATUS_REQUESTER = "urn:oasis:names:tc:SAML:2.0:status:Requester";

  public static final String URN_STATUS_RESPONDER = "urn:oasis:names:tc:SAML:2.0:status:Responder";

  public static final String URN_STATUS_SUCCESS = "urn:oasis:names:tc:SAML:2.0:status:Success";

  public static final String USE = "use";

  public static final String VALUE = "Value";

  public static final String VERSION = "Version";

  public static final String VERSION_2_0 = "2.0";

  public static final String WANT_ASSERTIONS_SIGNED = "WantAssertionsSigned";

  public static final String X_509 = "X.509";

  public static final String X509_CERTIFICATE = "X509Certificate";

  public static final String X509_DATA = "X509Data";

}
