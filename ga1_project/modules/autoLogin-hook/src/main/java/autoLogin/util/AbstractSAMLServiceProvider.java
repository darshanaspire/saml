package autoLogin.util;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auto.login.AutoLogin;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
// import com.liferay.portal.security.auth.AutoLogin;
// import com.liferay.portal.service.AssertionLocalServiceUtil;
// import com.liferay.portal.util.PortalUtil;
// import com.liferay.portal.util.PropsValues;

import saml.service.service.AssertionLocalServiceUtil;

import java.text.ParseException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public abstract class AbstractSAMLServiceProvider implements SAMLServiceProvider {

  @Override
  public boolean receiveResponse(HttpServletRequest request, String samlEntityId,
      String samlIdentityProviderId, String samlResponse) throws Exception {

    request.getSession().setAttribute(SAMLConstants.IDP_INTERROGATED, Boolean.TRUE);

    String relayState = request.getParameter(SAMLConstants.RELAY_STATE);
    _log.info("relayState-> " + relayState);
    if (Validator.isNull(relayState)) {
      // relayState = "/web/guest";
      relayState = "http://localhost:8000/";
    }

    request.setAttribute(AutoLogin.AUTO_LOGIN_REDIRECT, relayState);

    Date now = new Date();
    AssertionLocalServiceUtil.cleanDeprecatedAssertion(now);
    Document document = SAXReaderUtil.read(samlResponse);

    boolean authenticated = receiveResponseInternal(request, samlEntityId, samlIdentityProviderId,
        samlResponse, now, document);

    if (authenticated) {
      _log.info("Authenticated.");
      request.removeAttribute(AutoLogin.AUTO_LOGIN_REDIRECT);
      // request.setAttribute(AutoLogin.AUTO_LOGIN_REDIRECT_AND_AUTHENTICATE, relayState);
      request.setAttribute("AUTO_LOGIN_REDIRECT_AND_AUTHENTICATE", relayState);
    }
    _log.info("Not Authenticated.");
    return authenticated;
  }

  @Override
  public void sendAuthnRequest(HttpServletRequest request, HttpServletResponse response,
      String samlEntityId, String loginUrl, String signAlg) throws Exception {

    HttpSession session = request.getSession();

    String relayState = PortalUtil.getCurrentURL(request);
    for (String excluded : PropsValues.SAML_EXCLUDED_URL) {
      if (relayState.contains(excluded)) {
        return;
      }
    }

    boolean isLoginURL = false;
    for (String login : PropsValues.SAML_LOGIN_URL) {
      if (relayState.contains(login)) {
        isLoginURL = true;
        break;
      }
    }
    _log.info("RelayState --> " + relayState);
    Boolean isPassive;
    Object interrogated;
    _log.info("isLoginURL --> " + isLoginURL);
    if (isLoginURL) {
      interrogated = null;
      isPassive = null;
      relayState = SAMLUtil.getRelayStateFromLastPath(request);
      _log.info("RelayState --> " + relayState);
    } else {
      interrogated = session.getAttribute(SAMLConstants.IDP_INTERROGATED);
      isPassive = Boolean.TRUE;
      _log.info("RelayState ELSE  ");
    }

    if (interrogated == null || Boolean.FALSE.equals(interrogated)) {
      // session.setAttribute(SAMLConstants.IDP_INTERROGATED, Boolean.TRUE);
      // String userAgent = request.getHeader("user-agent");
      // if (_log.isDebugEnabled()) {
      // _log.debug("user-agent : " + userAgent);
      // }
      // for (String excluded : PropsValues.SAML_EXCLUDED_AGENT) {
      // if (userAgent.contains(excluded)) {
      // if (_log.isDebugEnabled()) {
      // _log.debug("user-agent excluded");
      // }
      // return;
      // }
      // }
      SAMLUtil.initHTTPHeader(response);

      sendAuthRequestInternal(request, samlEntityId, loginUrl, relayState, signAlg, isPassive);
    }

  }

  protected abstract boolean receiveResponseInternal(HttpServletRequest request,
      String samlEntityId, String samlIdentityProviderId, String samlResponse, Date now,
      Document document) throws ParseException, SystemException;

  protected abstract void sendAuthRequestInternal(HttpServletRequest request, String samlEntityId,
      String loginUrl, String relayState, String signAlg, Boolean isPassive) throws Exception;

  private static Log _log = LogFactoryUtil.getLog(AbstractSAMLServiceProvider.class);
}
