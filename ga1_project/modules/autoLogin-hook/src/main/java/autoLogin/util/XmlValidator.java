package autoLogin.util;

import java.io.ByteArrayOutputStream;

import java.io.IOException;
import java.util.zip.Inflater;
import java.util.zip.InflaterOutputStream;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Base64;



public class XmlValidator {
  private static Log log = LogFactoryUtil.getLog(XmlValidator.class);

  public static void decode_decompress(String string) {
    byte[] decodedBytes = Base64.decode(string);
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    Inflater decompresser = new Inflater(false);
    InflaterOutputStream inflaterOutputStream = new InflaterOutputStream(stream, decompresser);
    try {
      inflaterOutputStream.write(decodedBytes);
      inflaterOutputStream.close();
      String data = stream.toString();
      log.info("Data: {}" + data);
    } catch (IOException e) {
      log.info(string, e);
    }
  }
}
