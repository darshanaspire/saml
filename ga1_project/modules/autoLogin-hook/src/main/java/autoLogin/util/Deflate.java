package autoLogin.util;

import java.io.UnsupportedEncodingException;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

/**
 * 
 * @author aspire101
 *
 */
public class Deflate {

  public static byte[] encode(String string) throws UnsupportedEncodingException {

    Deflater lDeflater = new Deflater(Deflater.DEFAULT_COMPRESSION, true);
    lDeflater.setInput(string.getBytes(StringPool.UTF8));
    lDeflater.finish();

    int deflateLength;
    byte[] deflateBuffer = new byte[_BUFFER_SIZE];
    List bufferList = new ArrayList();
    while ((deflateLength = lDeflater.deflate(deflateBuffer)) == _BUFFER_SIZE) {

      bufferList.add(deflateBuffer);
      deflateBuffer = new byte[_BUFFER_SIZE];
    }

    byte[] deflate = new byte[bufferList.size() * _BUFFER_SIZE + deflateLength];

    for (Object buffer : bufferList) {
      System.arraycopy((byte) buffer, 0, deflate, 0, _BUFFER_SIZE);
    }

    System.arraycopy(deflateBuffer, 0, deflate, 0, deflateLength);

    return deflate;
  }

  public static String decode(byte raw[]) throws UnsupportedEncodingException, DataFormatException {

    Inflater inflater = new Inflater(true);
    inflater.setInput(raw);

    int resultLength;
    byte[] valueBytes = new byte[_BUFFER_SIZE];
    StringBundler inflateBuffer = new StringBundler();
    while ((resultLength = inflater.inflate(valueBytes)) != 0) {
      inflateBuffer.append(new String(valueBytes, 0, resultLength, StringPool.UTF8));
    }
    inflater.end();

    return inflateBuffer.toString();
  }

  private static final int _BUFFER_SIZE = 1024;


}
