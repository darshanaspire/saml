package autoLogin.util;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auto.login.AutoLogin;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Element;

import saml.service.service.AssertionLocalServiceUtil;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SAML2ServiceProvider extends AbstractSAMLServiceProvider {
  @Override
  protected boolean receiveResponseInternal(HttpServletRequest request, String samlEntityId,
      String samlIdentityProviderId, String samlResponse, Date now, Document document)
      throws ParseException, SystemException {

    boolean authenticated = false;

    HttpSession session = request.getSession();

    Element response = document.getRootElement();

    Element statusCode = response.element(SAMLConstants.STATUS).element(SAMLConstants.STATUS_CODE);
    if (SAMLConstants.URN_STATUS_SUCCESS.equals(statusCode.attributeValue(SAMLConstants.VALUE))
        && SAMLUtil.validateXmlSignature(samlResponse, false)) {

      // we are not going to validate XML signature
      // if
      // (SAMLConstants.URN_STATUS_SUCCESS.equals(statusCode.attributeValue(SAMLConstants.VALUE))) {
      String version = response.attributeValue(SAMLConstants.VERSION);
      if (version == null) {
        _log.error("response without version");
        return false;
      } else if (!version.startsWith("2.")) {
        _log.error("major response version number not supported");
        return false;
      }

      String ACSUrl = SAMLMetadataManager.getSPAssertionConsumerServiceLocation(request);

      String destination = response.attributeValue(SAMLConstants.DESTINATION);
      if (destination != null && !destination.equals(ACSUrl)) {
        _log.error(
            "destination must contains the service provider's assertion consumer service URL");
        return false;
      }

      Element issuer = response.element(SAMLConstants.ISSUER);
      if (issuer != null) {
        if (!issuer.getText().equals(samlIdentityProviderId)) {
          _log.error("incorrect issuer in response");
          return false;
        }
      }

      String messageID = (String) session.getAttribute(SAMLConstants.MESSAGE_ID);

      String inResponseTo = response.attributeValue(SAMLConstants.IN_RESPONSE_TO);
      if (inResponseTo != null) {
        if (messageID == null) {
          _log.error("inResponseTo must not be present for " + "unsolicited response");
          return false;
        } else if (messageID.equals(inResponseTo)) {
          session.removeAttribute(SAMLConstants.MESSAGE_ID);
        } else {
          _log.error("inResponseTo doesn't match the request's id");
          return false;
        }
      }

      Map<String, String> attributeValue = new HashMap<String, String>();
      request.setAttribute(SAMLConstants.ATTRIBUTE_VALUE_KEY, attributeValue);
      _log.info("This is before Assetion:::::::::::::::::::::::::");
      List<Element> assertions = response.elements(SAMLConstants.ASSERTION);
      assertions: for (Element assertion : assertions) {
        version = assertion.attributeValue(SAMLConstants.VERSION);
        if (version == null) {
          _log.error("assertion without version");
          continue;
        } else if (!version.startsWith("2.")) {
          _log.error("major assertion version number not supported");
          continue;
        }

        issuer = assertion.element(SAMLConstants.ISSUER);
        if (issuer == null) {
          _log.error("assertion without issuer");
          continue;
        } else if (!issuer.getText().equals(samlIdentityProviderId)) {
          _log.error("incorrect issuer in assertion");
          continue;
        }

        Element subject = assertion.element(SAMLConstants.SUBJECT);
        if (subject == null) {
          _log.error("assertion without subject");
          continue;
        }

        Element nameID = subject.element(SAMLConstants.NAME_ID);
        if (nameID != null) {
          session.setAttribute(SAMLConstants.NAME_ID_KEY, nameID.createCopy());
        }

        // FIXME encryptedID should be decrypted and re encrypted

        Element encryptedID = subject.element(SAMLConstants.ENCRYPTED_ID);
        if (encryptedID != null) {
          session.setAttribute(SAMLConstants.ENCRYPTED_ID_KEY, encryptedID.createCopy());
        }

        String assertionID = assertion.attributeValue(SAMLConstants.ID);

        boolean bearer = false;

        List<Element> subjectConfirmations = subject.elements(SAMLConstants.SUBJECT_CONFIRMATION);
        for (Element subjectConfirmation : subjectConfirmations) {
          if (SAMLConstants.SUBJECT_CONFIRMATION_METHOD_BEARER
              .equals(subjectConfirmation.attributeValue(SAMLConstants.METHOD))) {
            bearer = true;
            _log.info("Bearer:::::::" + bearer);
            Element subjectConfirmationData =
                subjectConfirmation.element(SAMLConstants.SUBJECT_CONFIRMATION_DATA);

            if (!ACSUrl.equals(subjectConfirmationData.attributeValue(SAMLConstants.RECIPIENT))) {
              _log.error(
                  "recipient must contains the service provider's assertion consumer service URL");
              continue assertions;
            }

            Date notOnOrAfter = SAMLUtil
                .parseDate(subjectConfirmationData.attributeValue(SAMLConstants.NOT_ON_OR_AFTER));
            _log.info("This is before Assetion:::::::::::::::::::::::::");
            if (notOnOrAfter == null) {
              _log.error("SubjectConfirmationData must contains NotOnOrAfter");
              continue assertions;
            } else if (notOnOrAfter.after(now)) {
              if (AssertionLocalServiceUtil.replayedAssertion(assertionID, notOnOrAfter)) {
                _log.error("assertion replayed");
                continue assertions;
              }
            } else {
              _log.error("SubjectConfirmationData NotOnOrAfter is passed");
              continue assertions;
            }
            _log.info("This is after Assetion:::::::::::::::::::::::::");
            inResponseTo = subjectConfirmationData.attributeValue(SAMLConstants.IN_RESPONSE_TO);
            if (inResponseTo != null) {
              if (messageID == null) {
                _log.error("inResponseTo must not be present for unsolicited response");
                continue assertions;
              } else if (!messageID.equals(inResponseTo)) {
                _log.error("inResponseTo doesn't match the request's id");
                continue assertions;
              }
            }
          }
        }
        _log.info("Authenticated: " + authenticated);
        Element conditions = assertion.element(SAMLConstants.CONDITIONS);
        if (conditions == null) {
          continue;
        }
        List<Element> audienceRestrictions =
            conditions.elements(SAMLConstants.AUDIENCE_RESTRICTION);
        for (Element audienceRestriction : audienceRestrictions) {
          List<Element> audiences = audienceRestriction.elements(SAMLConstants.AUDIENCE);
          boolean notFind = true;
          for (Element audience : audiences) {
            if (audience.getText().equals(samlEntityId)) {
              notFind = false;
              break;
            }
          }
          if (notFind) {
            continue assertions;
          }
        }

        List<Element> authnStatements = assertion.elements(SAMLConstants.AUTHN_STATEMENT);
        for (Element authnStatement : authnStatements) {
          session.setAttribute(SAMLConstants.SESSION_INDEX_KEY,
              authnStatement.attributeValue(SAMLConstants.SESSION_INDEX));
          String sessionNotOnOrAfter =
              authnStatement.attributeValue(SAMLConstants.SESSION_NOT_ON_OR_AFTER);
          if (sessionNotOnOrAfter != null) {
            session.setAttribute(SAMLConstants.SESSION_NOT_AFTER,
                SAMLUtil.parseDate(sessionNotOnOrAfter));
          }
          authenticated = bearer;
          break;
        }

        List<Element> attributeStatements = assertion.elements(SAMLConstants.ATTRIBUTE_STATEMENT);
        for (Element attributeStatement : attributeStatements) {
          List<Element> attributes = attributeStatement.elements(SAMLConstants.ATTRIBUTE);

          for (Element attribute : attributes) {
            String name = attribute.attributeValue(SAMLConstants.NAME);

            if (_log.isDebugEnabled()) {
              _log.debug("name : " + name);
            }

            List<Element> attributeValues = attribute.elements(SAMLConstants.ATTRIBUTE_VALUE);
            if (!attributeValues.isEmpty()) {
              String value = attributeValues.get(0).getText();
              attributeValue.put(name, value);

              if (_log.isDebugEnabled()) {
                _log.debug("value : " + value);
              }
            }
          }
        }
      }
    }

    return authenticated;
  }

  @Override
  protected void sendAuthRequestInternal(HttpServletRequest request, String samlEntityId,
      String loginUrl, String relayState, String signAlg, Boolean isPassive) throws Exception {

    request.setAttribute(AutoLogin.AUTO_LOGIN_REDIRECT, SAMLUtil.generateAuthnRequest(request,
        loginUrl, samlEntityId, relayState, signAlg, isPassive));
  }

  private static Log _log = LogFactoryUtil.getLog(SAML2ServiceProvider.class);

}
