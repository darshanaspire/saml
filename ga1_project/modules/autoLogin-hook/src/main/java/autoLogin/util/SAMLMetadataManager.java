package autoLogin.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Base64;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Element;
import com.liferay.portal.kernel.xml.Namespace;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;

import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateFactory;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import javax.xml.crypto.dsig.XMLSignature;

public class SAMLMetadataManager {



  public static String getIDPEntityID() {
    return _idpEntityID;
  }

  public static Certificate getIDPSigningCertificate() {
    return _idpSigningCertificate;
  }

  public static String getIDPSingleLogoutLocation() {
    return _idpSingleLogoutLocation;
  }

  public static String getIDPSingleLogoutResponseLocation() {
    return _idpSingleLogoutResponseLocation;
  }

  public static String getIDPSingleSignOnLocation() {
    return _idpSingleSignOnLocation;
  }

  public static SAMLServiceProvider getServiceProvider() {
    return _serviceProvider;
  }

  public static String getSPAssertionConsumerServiceLocation(HttpServletRequest request) {

    String portalURL = PortalUtil.getPortalURL(request);
    return portalURL.concat(SAMLConstants.ASSERTION_CONSUMER_SERVICE_URL);
  }

  public static Certificate getSPCertificate() {
    return _spCertificate;
  }

  public static PrivateKey getSPPrivateKey() {
    return _spPrivateKey;
  }

  public static String getSPMetadata(HttpServletRequest request)
      throws CertificateEncodingException {

    Document document = SAXReaderUtil.createDocument();

    Namespace md =
        SAXReaderUtil.createNamespace(SAMLConstants.MD, SAMLConstants.METADATA_NAMESPACE);
    Namespace ds = SAXReaderUtil.createNamespace(SAMLConstants.DS, XMLSignature.XMLNS);

    Element spEntityDescriptor =
        document.addElement(SAXReaderUtil.createQName(SAMLConstants.ENTITY_DESCRIPTOR, md));

    spEntityDescriptor.add(md);
    spEntityDescriptor.add(ds);

    spEntityDescriptor.addAttribute(SAMLConstants.ENTITY_ID, PropsValues.SAML_ENTITY_ID);

    Element spSSODescriptor = spEntityDescriptor
        .addElement(SAXReaderUtil.createQName(SAMLConstants.SP_SSO_DESCRIPTOR, md));

    spSSODescriptor.addAttribute(SAMLConstants.AUTHN_REQUESTS_SIGNED, StringPool.TRUE);
    spSSODescriptor.addAttribute(SAMLConstants.WANT_ASSERTIONS_SIGNED, StringPool.TRUE);
    spSSODescriptor.addAttribute(SAMLConstants.PROTOCOL_SUPPORT_ENUMERATION,
        SAMLConstants.PROTOCOL_NAMESPACE);

    Element keyDescriptor =
        spSSODescriptor.addElement(SAXReaderUtil.createQName(SAMLConstants.KEY_DESCRIPTOR, md));

    keyDescriptor.addAttribute(SAMLConstants.USE, SAMLConstants.SIGNING);

    Element keyInfo =
        keyDescriptor.addElement(SAXReaderUtil.createQName(SAMLConstants.KEY_INFO, ds));

    Element x509Data = keyInfo.addElement(SAXReaderUtil.createQName(SAMLConstants.X509_DATA, ds));

    Element x509Certificate =
        x509Data.addElement(SAXReaderUtil.createQName(SAMLConstants.X509_CERTIFICATE, ds));

    x509Certificate.addText(Base64.encode(getSPCertificate().getEncoded()));

    Element assertionConsumerService = spSSODescriptor
        .addElement(SAXReaderUtil.createQName(SAMLConstants.ASSERTION_CONSUMER_SERVICE, md));

    assertionConsumerService.addAttribute(SAMLConstants.IS_DEFAULT, StringPool.TRUE);
    assertionConsumerService.addAttribute(SAMLConstants.INDEX, "0");
    assertionConsumerService.addAttribute(SAMLConstants.BINDING, SAMLConstants.HTTP_POST);
    assertionConsumerService.addAttribute(SAMLConstants.LOCATION,
        getSPAssertionConsumerServiceLocation(request));

    Element singleLogoutService = spSSODescriptor
        .addElement(SAXReaderUtil.createQName(SAMLConstants.SINGLE_LOGOUT_SERVICE, md));

    singleLogoutService.addAttribute(SAMLConstants.BINDING, SAMLConstants.HTTP_REDIRECT);
    String location = getSPSingleLogoutLocation(request);
    singleLogoutService.addAttribute(SAMLConstants.LOCATION, location);
    singleLogoutService.addAttribute(SAMLConstants.RESPONSE_LOCATION, location);

    Element nameIdFormat =
        spSSODescriptor.addElement(SAXReaderUtil.createQName(SAMLConstants.NAME_ID_FORMAT, md));

    nameIdFormat.addText(SAMLConstants.NAME_ID_TRANSIENT_FORMAT);

    return document.asXML();
  }

  public static String getSPSingleLogoutLocation(HttpServletRequest request) {

    String portalURL = PortalUtil.getPortalURL(request);
    return portalURL.concat(SAMLConstants.SINGLE_LOGOUT_URL);
  }

  private static String _idpEntityID;

  private static Certificate _idpSigningCertificate;

  private static String _idpSingleLogoutLocation;

  private static String _idpSingleLogoutResponseLocation;

  private static String _idpSingleSignOnLocation;

  private static Log _log = LogFactoryUtil.getLog(SAMLMetadataManager.class);

  private static SAMLServiceProvider _serviceProvider;

  private static Certificate _spCertificate;

  private static PrivateKey _spPrivateKey;

  static {

    _idpEntityID = null;

    _idpSigningCertificate = null;

    _idpSingleLogoutLocation = null;

    _idpSingleLogoutResponseLocation = null;

    _idpSingleSignOnLocation = null;

    _serviceProvider = null;

    _spCertificate = null;

    _spPrivateKey = null;

    try {
      KeyStore keystore = KeyStore.getInstance(PropsValues.SAML_KEYSTORE_TYPE);
      keystore.load(new FileInputStream(PropsValues.SAML_KEYSTORE_FILE),
          PropsValues.SAML_KEYSTORE_PASS.toCharArray());

      _spCertificate = keystore.getCertificate(PropsValues.SAML_KEYSTORE_ALIAS);

      _spPrivateKey = (PrivateKey) keystore.getKey(PropsValues.SAML_KEYSTORE_ALIAS,
          PropsValues.SAML_KEYSTORE_KEY_PASS.toCharArray());
      _log.info("DARSHANNNNNNN");
      _log.info(SAMLConstants.VERSION_2_0 + "   ==   " + PropsValues.SAML_VERSION);
      if (SAMLConstants.VERSION_2_0.equals(PropsValues.SAML_VERSION)) {
        _serviceProvider = new SAML2ServiceProvider();

        if (_log.isDebugEnabled()) {
          _log.debug("parse SAML 2 metadata from : " + PropsValues.SAML_IDENTITY_PROVIDER_METADATA);
        }

        Document document = SAXReaderUtil
            .read(new FileInputStream(PropsValues.SAML_IDENTITY_PROVIDER_METADATA), true);

        Element idpEntityDescriptor = document.getRootElement();

        _idpEntityID = idpEntityDescriptor.attributeValue(SAMLConstants.ENTITY_ID);
        if (_log.isDebugEnabled()) {
          _log.debug("entityID : " + _idpEntityID);
        }

        Element idpSSODescriptor = idpEntityDescriptor.element(SAMLConstants.IDP_SSO_DESCRIPTOR);
        List<Element> keyDescriptors = idpSSODescriptor.elements(SAMLConstants.KEY_DESCRIPTOR);

        byte[] certificateData = null;

        for (Element keyDescriptor : keyDescriptors) {
          String use = keyDescriptor.attributeValue(SAMLConstants.USE);
          if (use == null || SAMLConstants.SIGNING.equals(use)) {
            Element keyInfo = keyDescriptor.element(SAMLConstants.KEY_INFO);
            Element x509Data = keyInfo.element(SAMLConstants.X509_DATA);
            String x509Certificate = x509Data.elementText(SAMLConstants.X509_CERTIFICATE);

            // we remove new line which are allowed in
            // <X509Certificate>, but not supported by
            // the base64 codec

            x509Certificate = x509Certificate.replaceAll(StringPool.NEW_LINE, StringPool.BLANK);
            certificateData = Base64.decode(x509Certificate);
            break;
          }
        }

        CertificateFactory cf = CertificateFactory.getInstance(SAMLConstants.X_509);
        Collection<? extends Certificate> colCert =
            cf.generateCertificates(new ByteArrayInputStream(certificateData));

        _idpSigningCertificate = colCert.iterator().next();
        if (_log.isDebugEnabled()) {
          _log.debug("certificate : " + _idpSigningCertificate);
        }

        List<Element> singleLogoutServices =
            idpSSODescriptor.elements(SAMLConstants.SINGLE_LOGOUT_SERVICE);

        for (Element singleLogoutService : singleLogoutServices) {
          if (SAMLConstants.HTTP_REDIRECT
              .equals(singleLogoutService.attributeValue(SAMLConstants.BINDING))) {
            _idpSingleLogoutLocation = singleLogoutService.attributeValue(SAMLConstants.LOCATION);
            _idpSingleLogoutResponseLocation =
                singleLogoutService.attributeValue(SAMLConstants.RESPONSE_LOCATION);
            if (_idpSingleLogoutResponseLocation == null) {
              _idpSingleLogoutResponseLocation = _idpSingleLogoutLocation;
            }
            break;
          }
        }

        List<Element> singleSignOnServices =
            idpSSODescriptor.elements(SAMLConstants.SINGLE_SIGN_ON_SERVICE);

        for (Element singleSignOnService : singleSignOnServices) {
          if (SAMLConstants.HTTP_REDIRECT
              .equals(singleSignOnService.attributeValue(SAMLConstants.BINDING))) {
            _idpSingleSignOnLocation = singleSignOnService.attributeValue(SAMLConstants.LOCATION);
            break;
          }
        }
      } else {
        _serviceProvider = new SAML11ServiceProvider();

        _idpEntityID = PropsValues.SAML_IDENTITY_PROVIDER_ID;

        _idpSingleSignOnLocation = PropsValues.SAML_INTER_SITE_TRANSFER_SERVICE_URL;

        _idpSigningCertificate = keystore.getCertificate(PropsValues.SAML_KEYSTORE_IDP_ALIAS);

      }
    } catch (Exception e) {
      _log.error(e, e);
    }
  }



}
