package autoLogin.util;

import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsUtil;

public class PropsValues {
  public static final int RSS_CONNECTION_TIMEOUT =
      GetterUtil.getInteger(PropsKeys.RSS_CONNECTION_TIMEOUT);
  public static final String[] SAML_ADDRESS_CITY_ATTRS =
      PropsUtil.getArray(PropsKeys.SAML_ADDRESS_CITY_ATTRS);
  public static final String[] SAML_ADDRESS_COUNTRY_ATTRS =
      PropsUtil.getArray(PropsKeys.SAML_ADDRESS_COUNTRY_ATTRS);
  public static final long SAML_ADDRESS_DEFAULT_COUNTRY_ID =
      GetterUtil.getLong(PropsUtil.get(PropsKeys.SAML_ADDRESS_DEFAULT_COUNTRY_ID));
  public static final String[] SAML_ADDRESS_POSTALCODE_ATTRS =
      PropsUtil.getArray(PropsKeys.SAML_ADDRESS_POSTALCODE_ATTRS);
  public static final String[] SAML_ADDRESS_REGION_ATTRS =
      PropsUtil.getArray(PropsKeys.SAML_ADDRESS_REGION_ATTRS);
  public static final String[] SAML_ADDRESS_STREET_ATTRS =
      PropsUtil.getArray(PropsKeys.SAML_ADDRESS_STREET_ATTRS);
  public static final String[] SAML_ADDRESS_TYPES_ID =
      PropsUtil.getArray(PropsKeys.SAML_ADDRESS_TYPES_ID);
  public static final boolean SAML_AUTH_ENABLED =
      GetterUtil.getBoolean(PropsUtil.get(PropsKeys.SAML_AUTH_ENABLED));
  public static final String SAML_BIRTH_DAY_ATTR = PropsUtil.get(PropsKeys.SAML_BIRTH_DAY_ATTR);
  public static final String SAML_BIRTH_DAY_FORMAT = PropsUtil.get(PropsKeys.SAML_BIRTH_DAY_FORMAT);
  public static final String SAML_EMAIL_ADDRESS_ATTR =
      PropsUtil.get(PropsKeys.SAML_EMAIL_ADDRESS_ATTR);
  public static final String SAML_ENTITY_ID = PropsUtil.get(PropsKeys.SAML_ENTITY_ID);
  public static final String[] SAML_EXCLUDED_AGENT =
      PropsUtil.getArray(PropsKeys.SAML_EXCLUDED_AGENT);
  public static final String[] SAML_EXCLUDED_URL = PropsUtil.getArray(PropsKeys.SAML_EXCLUDED_URL);
  public static final String SAML_FIRST_NAME_ATTR = PropsUtil.get(PropsKeys.SAML_FIRST_NAME_ATTR);
  public static final String SAML_IDENTITY_PROVIDER_ID =
      PropsUtil.get(PropsKeys.SAML_IDENTITY_PROVIDER_ID);
  public static final String SAML_IDENTITY_PROVIDER_METADATA =
      PropsUtil.get(PropsKeys.SAML_IDENTITY_PROVIDER_METADATA);
  public static final String SAML_INTER_SITE_TRANSFER_SERVICE_URL =
      PropsUtil.get(PropsKeys.SAML_INTER_SITE_TRANSFER_SERVICE_URL);
  public static final String SAML_KEYSTORE_ALIAS = PropsUtil.get(PropsKeys.SAML_KEYSTORE_ALIAS);
  public static final String SAML_KEYSTORE_FILE = PropsUtil.get(PropsKeys.SAML_KEYSTORE_FILE);
  public static final String SAML_KEYSTORE_IDP_ALIAS =
      PropsUtil.get(PropsKeys.SAML_KEYSTORE_IDP_ALIAS);
  public static final String SAML_KEYSTORE_KEY_PASS =
      PropsUtil.get(PropsKeys.SAML_KEYSTORE_KEY_PASS);
  public static final String SAML_KEYSTORE_PASS = PropsUtil.get(PropsKeys.SAML_KEYSTORE_PASS);
  public static final String SAML_KEYSTORE_TYPE = PropsUtil.get(PropsKeys.SAML_KEYSTORE_TYPE);
  public static final String SAML_LAST_NAME_ATTR = PropsUtil.get(PropsKeys.SAML_LAST_NAME_ATTR);
  public static final boolean SAML_LDAP_IMPORT_ENABLED =
      GetterUtil.getBoolean(PropsUtil.get(PropsKeys.SAML_LDAP_IMPORT_ENABLED));
  public static final String[] SAML_LOGIN_URL = PropsUtil.getArray(PropsKeys.SAML_LOGIN_URL);
  public static final String[] SAML_PHONE_ATTRS = PropsUtil.getArray(PropsKeys.SAML_PHONE_ATTRS);
  public static final String[] SAML_PHONE_TYPES_ID =
      PropsUtil.getArray(PropsKeys.SAML_PHONE_TYPES_ID);
  public static final String SAML_REDIRECT_SIGN_ALG =
      PropsUtil.get(PropsKeys.SAML_REDIRECT_SIGN_ALG);
  public static final String SAML_SCREEN_NAME_ATTR = PropsUtil.get(PropsKeys.SAML_SCREEN_NAME_ATTR);
  public static final String SAML_VERSION = PropsUtil.get(PropsKeys.SAML_VERSION);
  public static final String SANDBOX_DEPLOY_DIR = PropsUtil.get(PropsKeys.SANDBOX_DEPLOY_DIR);

  public static final boolean SANDBOX_DEPLOY_ENABLED =
      GetterUtil.getBoolean(PropsUtil.get(PropsKeys.SANDBOX_DEPLOY_ENABLED));

  public static final boolean AUTH_FORWARD_BY_LAST_PATH =
      GetterUtil.getBoolean(PropsUtil.get(PropsKeys.AUTH_FORWARD_BY_LAST_PATH));
}
