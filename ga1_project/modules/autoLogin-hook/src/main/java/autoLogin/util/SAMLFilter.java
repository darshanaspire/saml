// package autoLogin.util;
//
// import java.io.IOException;
// import javax.servlet.http.HttpServletRequest;
// import javax.servlet.http.HttpServletResponse;
// import javax.servlet.FilterChain;
// import java.io.UnsupportedEncodingException;
// import java.security.GeneralSecurityException;
// import java.util.Date;
// import java.util.zip.DataFormatException;
//
// import javax.servlet.FilterChain;
// import javax.servlet.ServletOutputStream;
// import javax.servlet.http.HttpServletRequest;
// import javax.servlet.http.HttpServletResponse;
// import javax.servlet.http.HttpSession;
//
// import com.liferay.portal.kernel.exception.SystemException;
// import com.liferay.portal.kernel.log.Log;
// import com.liferay.portal.kernel.log.LogFactoryUtil;
// import com.liferay.portal.kernel.util.GetterUtil;
// import com.liferay.portal.kernel.util.PortalUtil;
// import com.liferay.portal.kernel.util.PrefsPropsUtil;
// import com.liferay.portal.kernel.util.Validator;
// import com.liferay.portal.kernel.xml.Document;
// import com.liferay.portal.kernel.xml.DocumentException;
// import com.liferay.portal.kernel.xml.Element;
// import com.liferay.portal.kernel.xml.Namespace;
// import com.liferay.portal.kernel.xml.SAXReaderUtil;
//
// public class SAMLFilter extends BasePortalFilter {
//
// @Override
// protected Log getLog() {
// // TODO Auto-generated method stub
// return null;
// }
//
// protected void processFilter(HttpServletRequest request, HttpServletResponse response,
// FilterChain filterChain) throws Exception {
//
// long companyId = PortalUtil.getCompanyId(request);
//
// boolean enabled = PrefsPropsUtil.getBoolean(companyId, PropsKeys.SAML_AUTH_ENABLED,
// PropsValues.SAML_AUTH_ENABLED);
// String logoutUrl = SAMLMetadataManager.getIDPSingleLogoutResponseLocation();
// String samlEntityId =
// PrefsPropsUtil.getString(companyId, PropsKeys.SAML_ENTITY_ID, PropsValues.SAML_ENTITY_ID);
// String samlIdentityProvider = SAMLMetadataManager.getIDPEntityID();
// String signAlg = PrefsPropsUtil.getString(companyId, PropsKeys.SAML_REDIRECT_SIGN_ALG,
// PropsValues.SAML_REDIRECT_SIGN_ALG);
//
// // display metadata or receive logout request and response
// // only if saml 2.0 is enabled (saml 1.1 doesn't support them)
//
// if (!(enabled && SAMLConstants.VERSION_2_0.equals(PropsValues.SAML_VERSION))
// || Validator.isNull(logoutUrl)) {
//
// processFilter(SAMLFilter.class, request, response, filterChain);
//
// return;
// }
//
// String requestURI = GetterUtil.getString(request.getRequestURI());
//
// if (requestURI.endsWith(SAMLConstants.SINGLE_LOGOUT_URL)) {
//
// if (SAMLUtil.validateUrlParameterSignature(request)) {
// String samlResponseParameter = request.getParameter(SAMLConstants.SAML_RESPONSE);
// if (samlResponseParameter != null) {
// _receiveLogoutResponse(request, samlIdentityProvider, samlResponseParameter);
// } else {
//
// String samlRequestParameter = request.getParameter(SAMLConstants.SAML_REQUEST);
// if (samlRequestParameter != null) {
// _receiveLogoutRequest(request, response, logoutUrl, samlEntityId, samlIdentityProvider,
// signAlg, samlRequestParameter);
//
// return;
// }
// }
// } else {
// _log.error("couldn't validate signature");
// }
//
// String relayState = request.getParameter(SAMLConstants.RELAY_STATE);
// if (Validator.isNull(relayState)) {
// relayState = PortalUtil.getPathMain();
// }
//
// SAMLUtil.initHTTPHeader(response);
//
// response.sendRedirect(relayState);
// } else {
// ServletOutputStream outputStream = response.getOutputStream();
// outputStream.print(SAMLMetadataManager.getSPMetadata(request));
// }
// }
//
// private boolean _isValideLogoutRequest(HttpServletRequest request, String samlRequest,
// String samlIdentityProvider) throws DocumentException {
//
// HttpSession session = request.getSession();
//
// Document requestDocument = SAXReaderUtil.read(samlRequest);
//
// Element logoutRequest = requestDocument.getRootElement();
//
// String destination = logoutRequest.attributeValue(SAMLConstants.DESTINATION);
//
// Element requestIssuer = logoutRequest.element(SAMLConstants.ISSUER);
//
// Element nameID = (Element) session.getAttribute(SAMLConstants.NAME_ID_KEY);
// Element encryptedID = (Element) session.getAttribute(SAMLConstants.ENCRYPTED_ID_KEY);
//
// if (!(destination == null
// || destination.equals(SAMLMetadataManager.getSPSingleLogoutLocation(request)))) {
//
// return false;
// }
//
// if (requestIssuer == null || !requestIssuer.getText().equals(samlIdentityProvider)) {
//
// return false;
// }
//
// if (nameID != null
// && nameID.getText().equals(logoutRequest.element(SAMLConstants.NAME_ID).getText())) {
//
// return true;
// }
//
// if (encryptedID != null
// && encryptedID.equals(logoutRequest.element(SAMLConstants.ENCRYPTED_ID))) {
//
// return true;
// }
//
// return false;
// }
//
// private void _receiveLogoutRequest(HttpServletRequest request, HttpServletResponse response,
// String logoutUrl, String samlEntityId, String samlIdentityProvider, String signAlg,
// String samlRequestParameter) throws DataFormatException, UnsupportedEncodingException,
// IOException, SystemException, GeneralSecurityException, DocumentException {
//
// HttpSession session = request.getSession();
//
// String samlRequest = SAMLUtil.getSamlFromHTTPRedirectBindingParameter(samlRequestParameter);
//
// String relayState = request.getParameter(SAMLConstants.RELAY_STATE);
//
// String statusValue;
// if (_isValideLogoutRequest(request, samlIdentityProvider, samlRequest)) {
//
// try {
// session.invalidate();
// statusValue = SAMLConstants.URN_STATUS_SUCCESS;
// } catch (IllegalStateException ise) {
// _log.error(ise, ise);
// statusValue = SAMLConstants.URN_STATUS_RESPONDER;
// }
// } else {
// statusValue = SAMLConstants.URN_STATUS_REQUESTER;
// }
//
// Document responseDocument = SAXReaderUtil.createDocument();
//
// Namespace samlp =
// SAXReaderUtil.createNamespace(SAMLConstants.SAMLP, SAMLConstants.PROTOCOL_NAMESPACE);
// Namespace saml =
// SAXReaderUtil.createNamespace(SAMLConstants.SAML, SAMLConstants.ASSERTION_NAMESPACE);
//
// Element logoutResponse = responseDocument
// .addElement(SAXReaderUtil.createQName(SAMLConstants.LOGOUT_RESPONSE, samlp));
//
// logoutResponse.add(samlp);
// logoutResponse.add(saml);
//
// logoutResponse.addAttribute(SAMLConstants.ID, SAMLUtil.generateMessageID());
// logoutResponse.addAttribute(SAMLConstants.VERSION, SAMLConstants.VERSION_2_0);
// logoutResponse.addAttribute(SAMLConstants.ISSUE_INSTANT, SAMLUtil.formatDate(new Date()));
// logoutResponse.addAttribute(SAMLConstants.DESTINATION, logoutUrl);
//
// Element responseIssuer =
// logoutResponse.addElement(SAXReaderUtil.createQName(SAMLConstants.ISSUER, saml));
// responseIssuer.setText(samlEntityId);
//
// Element status =
// logoutResponse.addElement(SAXReaderUtil.createQName(SAMLConstants.STATUS, samlp));
//
// Element statusCode =
// status.addElement(SAXReaderUtil.createQName(SAMLConstants.STATUS_CODE, samlp));
// statusCode.addAttribute(SAMLConstants.VALUE, statusValue);
//
// String xmlString = responseDocument.asXML();
// if (_log.isDebugEnabled()) {
// _log.debug("send : " + xmlString);
// }
//
// SAMLUtil.initHTTPHeader(response);
//
// response.sendRedirect(
// SAMLUtil.getHTTPRedirectBindingUrl(logoutUrl, false, xmlString, relayState, signAlg));
// }
//
// private void _receiveLogoutResponse(HttpServletRequest request, String samlIdentityProvider,
// String samlResponseParameter)
// throws DataFormatException, DocumentException, UnsupportedEncodingException {
//
// HttpSession session = request.getSession();
//
// String samlResponse = SAMLUtil.getSamlFromHTTPRedirectBindingParameter(samlResponseParameter);
//
// Document document = SAXReaderUtil.read(samlResponse);
//
// Element logoutResponse = document.getRootElement();
//
// String destination = logoutResponse.attributeValue(SAMLConstants.DESTINATION);
//
// Element issuer = logoutResponse.element(SAMLConstants.ISSUER);
// if (issuer == null) {
// _log.error("logoutResponse without issuer");
// } else if (!issuer.getText().equals(samlIdentityProvider)) {
// _log.error("incorrect issuer in logoutResponse");
// } else if (destination != null
// && !destination.equals(SAMLMetadataManager.getSPSingleLogoutLocation(request))) {
// _log.error("destination must contains the service " + "provider's single logout URL");
// } else {
//
// // we were waiting from IdP for invalidate session,
// // and it have correctly finished session on other SP
//
// Element statusCode =
// logoutResponse.element(SAMLConstants.STATUS).element(SAMLConstants.STATUS_CODE);
// if (SAMLConstants.URN_STATUS_SUCCESS.equals(statusCode.attributeValue(SAMLConstants.VALUE))
// && Boolean.TRUE.equals(session.getAttribute(PropsKeys.INVALIDATE_SESSION_FROM_IDP))) {
// session.invalidate();
// }
// }
// }
//
// private static Log _log = LogFactoryUtil.getLog(SAMLFilter.class);
// }
