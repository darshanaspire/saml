package autoLogin.hook;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Signature;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.crypto.dsig.SignatureMethod;

import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Contact;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auto.login.AutoLogin;
import com.liferay.portal.kernel.security.auto.login.AutoLoginException;
import com.liferay.portal.kernel.service.ContactLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Base64;
import com.liferay.portal.kernel.util.HttpUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PwdGenerator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Element;
import com.liferay.portal.kernel.xml.Namespace;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

import autoLogin.util.Deflate;
import autoLogin.util.PropsKeys;
import autoLogin.util.PropsValues;
import autoLogin.util.SAMLConstants;
import autoLogin.util.SAMLMetadataManager;
import autoLogin.util.SAMLUtil;

/**
 * 
 * @author aspire101
 *
 */
public class SamlAutoLogin implements AutoLogin {
  private static Log _log = LogFactoryUtil.getLog(SamlAutoLogin.class);


  @Override
  public String[] login(HttpServletRequest request, HttpServletResponse response) {



    // String string =
    // "PHNhbWxwOlJlc3BvbnNlIHhtbG5zOnNhbWxwPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6cHJvdG9jb2wiIHhtbG5zOnNhbWw9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iIElEPSJfNzBlYjMyZDcxYTIwODUxZDAzNmZkYWQzY2EyZDAyOWM5NTJhYjMxZjViIiBWZXJzaW9uPSIyLjAiIElzc3VlSW5zdGFudD0iMjAxOS0xMS0wMlQxMDozNzozNFoiIERlc3RpbmF0aW9uPSJodHRwOi8vbG9jYWxob3N0OjgwMDAvZ3JvdXAvZ3Vlc3QvcGF5bWVudCIgSW5SZXNwb25zZVRvPSJ6algzR053UDk1ZUNJVVBmYXhhV2VrdERxZXVlIj48c2FtbDpJc3N1ZXI+aHR0cDovL2xvY2FsaG9zdDo4MDgwL3NpbXBsZXNhbWwvc2FtbDIvaWRwL21ldGFkYXRhLnBocDwvc2FtbDpJc3N1ZXI+PGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+CiAgPGRzOlNpZ25lZEluZm8+PGRzOkNhbm9uaWNhbGl6YXRpb25NZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz4KICAgIDxkczpTaWduYXR1cmVNZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjcnNhLXNoYTEiLz4KICA8ZHM6UmVmZXJlbmNlIFVSST0iI183MGViMzJkNzFhMjA4NTFkMDM2ZmRhZDNjYTJkMDI5Yzk1MmFiMzFmNWIiPjxkczpUcmFuc2Zvcm1zPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48L2RzOlRyYW5zZm9ybXM+PGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNzaGExIi8+PGRzOkRpZ2VzdFZhbHVlPlBoT2s2RHlyNU5DcFJjeE5GUE1kTTRTVFdUWT08L2RzOkRpZ2VzdFZhbHVlPjwvZHM6UmVmZXJlbmNlPjwvZHM6U2lnbmVkSW5mbz48ZHM6U2lnbmF0dXJlVmFsdWU+WmRFYjkrenhacWVPcy9pa080aHE3UmZhaFRPRVM3dUh1eWdsK3ZLTytERENYcDZKSEkwUTkxbVJOdFVobkhNWm9TUE42bjNaVTU4azJDcG90TjloYW14K2hVZUFpZXFQUjZnVVBEQ2U3RmZKK0o5Yy9WWm5WbHZlakpiem5IN01DUzhKaU1VeXRxbndYY1U2blcvWlBBZTBKNWFxVjlkem9sVjZNQXdHdFRUOUtTWDlMR2FFOGZuUkxPS2dtcmF4V2lTSG96TUxHWUcxVW0xKzJROFBUSFZUbHpaVWd4Q2IzbGxjKzEwSERFU2cxaGdFSUJaSHBrZS8rMWxlV05aVENhZi9CTStyRDBPMFZTNUgzSldhZWYxZllWSjIyb3pEeFdpWmtVUWF4d0kxMmZPdkxsdndMN000bWlta3J4UzFxNFMzOVlJeWRTcklFMitadW0zVFBnPT08L2RzOlNpZ25hdHVyZVZhbHVlPgo8ZHM6S2V5SW5mbz48ZHM6WDUwOURhdGE+PGRzOlg1MDlDZXJ0aWZpY2F0ZT5NSUlEWFRDQ0FrV2dBd0lCQWdJSkFMbVZWdURXdTROWU1BMEdDU3FHU0liM0RRRUJDd1VBTUVVeEN6QUpCZ05WQkFZVEFrRlZNUk13RVFZRFZRUUlEQXBUYjIxbExWTjBZWFJsTVNFd0h3WURWUVFLREJoSmJuUmxjbTVsZENCWGFXUm5hWFJ6SUZCMGVTQk1kR1F3SGhjTk1UWXhNak14TVRRek5EUTNXaGNOTkRnd05qSTFNVFF6TkRRM1dqQkZNUXN3Q1FZRFZRUUdFd0pCVlRFVE1CRUdBMVVFQ0F3S1UyOXRaUzFUZEdGMFpURWhNQjhHQTFVRUNnd1lTVzUwWlhKdVpYUWdWMmxrWjJsMGN5QlFkSGtnVEhSa01JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBelVDRm96Z05iMWgxTTBqek5SU0NqaE9CblIrdVZiVnBhV2ZYWUlSK0FoV0RkRWU1cnlZK0NnYXZPZzhiZkx5Ynl6RmRlaGxZZERSZ2tlZEVCL0dqRzhhSncwNmwwcUY0akRPQXcwa0V5Z1dDdTJtY0g3WE94UnQrWUFIM1RWSGEvSHUxVzNXanprb2JxcXFMUThna0tXV00yN2ZPZ0FaNkdpZWFKQk42VkJTTU1jUGV5M0hXTEJtYytUWUptdjFkYmFPMmpIaEtoOHBmS3cwVzEyVk04UDFQSU84Z3Y0UGh1L3V1SllpZUJXS2l4QkV5eTBsSGp5aXhZRkNSMTJ4ZGg0Q0E0N3E5NThaUkdubkRVR0ZWRTFRaGdSYWNKQ09aOWJkNXQ5bXI4S0xhVkJZVENKbzVFUkU4anltYWI1ZFBxZTVxS2ZKc0NaaXFXZ2xialVvOXR3SURBUUFCbzFBd1RqQWRCZ05WSFE0RUZnUVV4cHV3Y3MvQ1lRT3l1aStyMUcrM0t4Qk5oeGt3SHdZRFZSMGpCQmd3Rm9BVXhwdXdjcy9DWVFPeXVpK3IxRyszS3hCTmh4a3dEQVlEVlIwVEJBVXdBd0VCL3pBTkJna3Foa2lHOXcwQkFRc0ZBQU9DQVFFQUFpV1VLcy8yeC92aU5DS2kzWTZibEV1Q3RBR2h6T09aOUVqcnZKOCtDT0gzUmFnM3RWQldyY0JaMy91aGhQcTVneTlscXc0T2t2RXdzOTkvNWpGc1gxRko2TUtCZ3FmdXk3eWg1czFZZk0wQU5IWWN6TW1ZcFplQWNRZjJDR0FhVmZ3VFRmU2x6TkxzRjJsVy9seTd5YXBGemxZU0pMR29WRStPSEV1OGc1U2xOQUNVRWZrWHcrNUVnaGgrS3psSU43UjZRN3IyaXhXTkZCQy9qV2Y3TktVZkp5WDhxSUc1bWQxWVVlVDZHQlc5Qm0yLzEvUmlPMjRKVGFZbGZMZEtLOVRZYjhzRzVCK09MYWIyREltRzk5Q0oyNVJrQWNTb2JXTkY1ekQwTzZsZ09vM2NFZEIva3NDcTNobXRsQy9EbExaL0Q4Q0orN1Z1Wm5TMXJSMm5hUT09PC9kczpYNTA5Q2VydGlmaWNhdGU+PC9kczpYNTA5RGF0YT48L2RzOktleUluZm8+PC9kczpTaWduYXR1cmU+PHNhbWxwOlN0YXR1cz48c2FtbHA6U3RhdHVzQ29kZSBWYWx1ZT0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnN0YXR1czpTdWNjZXNzIi8+PC9zYW1scDpTdGF0dXM+PHNhbWw6QXNzZXJ0aW9uIHhtbG5zOnhzaT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEtaW5zdGFuY2UiIHhtbG5zOnhzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYSIgSUQ9Il84Yjc1NGFkYjkxYTczYjczN2EwYzgzYWVmNmFmNjk3MjFjZjE4N2IzMGQiIFZlcnNpb249IjIuMCIgSXNzdWVJbnN0YW50PSIyMDE5LTExLTAyVDEwOjM3OjM0WiI+PHNhbWw6SXNzdWVyPmh0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9zaW1wbGVzYW1sL3NhbWwyL2lkcC9tZXRhZGF0YS5waHA8L3NhbWw6SXNzdWVyPjxkczpTaWduYXR1cmUgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPgogIDxkczpTaWduZWRJbmZvPjxkczpDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIi8+CiAgICA8ZHM6U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3JzYS1zaGExIi8+CiAgPGRzOlJlZmVyZW5jZSBVUkk9IiNfOGI3NTRhZGI5MWE3M2I3MzdhMGM4M2FlZjZhZjY5NzIxY2YxODdiMzBkIj48ZHM6VHJhbnNmb3Jtcz48ZHM6VHJhbnNmb3JtIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI2VudmVsb3BlZC1zaWduYXR1cmUiLz48ZHM6VHJhbnNmb3JtIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIi8+PC9kczpUcmFuc2Zvcm1zPjxkczpEaWdlc3RNZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjc2hhMSIvPjxkczpEaWdlc3RWYWx1ZT51aXZ2N2dkdjdYdy9YQ1dJMkF2cWZlNXYwK2M9PC9kczpEaWdlc3RWYWx1ZT48L2RzOlJlZmVyZW5jZT48L2RzOlNpZ25lZEluZm8+PGRzOlNpZ25hdHVyZVZhbHVlPkhyU3dZaUN1TmRiclRjbDNZU0hXSWVQSmM1encvb1VNVlJPdVhCWlYySFRtdkRYYUFzS1B4QS94Z1ZhYjFtbU5jS2lYaWgxWDhjd3ZkTHZBOXpxaWFML2tQcnAwd0lBbWhjbkhmdTQwbHJMbE1vZ3FiNVhSYTVOUHgwSzk1ZWZwSFpId3h2ZGErVGRQQmNoUTd6OS8zSnFkdE9CN1hsTjJObWVIWmp3ODhaWjRwb2piVU1MQ250U25MUWhmQWNSRzhNM3IxZDZaOHJBRTArS1VpSGdFYnFqdFByc01vck1YdHVEa1E4bEdVYnVVbWFPRXJ5M0NIQ2RTY1pwRUkrK2RlL3JlWGR3a3JQckYzUEhHZzVsVkIwRWdQQ0ViTjZMeE8rSHcyUHBVMGVrTVovcE5GbGxobmtOc1YrZkwrZEZDSVdhSUw0QUdoWk1rYkxvVWs3a1N0Zz09PC9kczpTaWduYXR1cmVWYWx1ZT4KPGRzOktleUluZm8+PGRzOlg1MDlEYXRhPjxkczpYNTA5Q2VydGlmaWNhdGU+TUlJRFhUQ0NBa1dnQXdJQkFnSUpBTG1WVnVEV3U0TllNQTBHQ1NxR1NJYjNEUUVCQ3dVQU1FVXhDekFKQmdOVkJBWVRBa0ZWTVJNd0VRWURWUVFJREFwVGIyMWxMVk4wWVhSbE1TRXdId1lEVlFRS0RCaEpiblJsY201bGRDQlhhV1JuYVhSeklGQjBlU0JNZEdRd0hoY05NVFl4TWpNeE1UUXpORFEzV2hjTk5EZ3dOakkxTVRRek5EUTNXakJGTVFzd0NRWURWUVFHRXdKQlZURVRNQkVHQTFVRUNBd0tVMjl0WlMxVGRHRjBaVEVoTUI4R0ExVUVDZ3dZU1c1MFpYSnVaWFFnVjJsa1oybDBjeUJRZEhrZ1RIUmtNSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DQVE4QU1JSUJDZ0tDQVFFQXpVQ0ZvemdOYjFoMU0wanpOUlNDamhPQm5SK3VWYlZwYVdmWFlJUitBaFdEZEVlNXJ5WStDZ2F2T2c4YmZMeWJ5ekZkZWhsWWREUmdrZWRFQi9Hakc4YUp3MDZsMHFGNGpET0F3MGtFeWdXQ3UybWNIN1hPeFJ0K1lBSDNUVkhhL0h1MVczV2p6a29icXFxTFE4Z2tLV1dNMjdmT2dBWjZHaWVhSkJONlZCU01NY1BleTNIV0xCbWMrVFlKbXYxZGJhTzJqSGhLaDhwZkt3MFcxMlZNOFAxUElPOGd2NFBodS91dUpZaWVCV0tpeEJFeXkwbEhqeWl4WUZDUjEyeGRoNENBNDdxOTU4WlJHbm5EVUdGVkUxUWhnUmFjSkNPWjliZDV0OW1yOEtMYVZCWVRDSm81RVJFOGp5bWFiNWRQcWU1cUtmSnNDWmlxV2dsYmpVbzl0d0lEQVFBQm8xQXdUakFkQmdOVkhRNEVGZ1FVeHB1d2NzL0NZUU95dWkrcjFHKzNLeEJOaHhrd0h3WURWUjBqQkJnd0ZvQVV4cHV3Y3MvQ1lRT3l1aStyMUcrM0t4Qk5oeGt3REFZRFZSMFRCQVV3QXdFQi96QU5CZ2txaGtpRzl3MEJBUXNGQUFPQ0FRRUFBaVdVS3MvMngvdmlOQ0tpM1k2YmxFdUN0QUdoek9PWjlFanJ2SjgrQ09IM1JhZzN0VkJXcmNCWjMvdWhoUHE1Z3k5bHF3NE9rdkV3czk5LzVqRnNYMUZKNk1LQmdxZnV5N3loNXMxWWZNMEFOSFljek1tWXBaZUFjUWYyQ0dBYVZmd1RUZlNsek5Mc0YybFcvbHk3eWFwRnpsWVNKTEdvVkUrT0hFdThnNVNsTkFDVUVma1h3KzVFZ2hoK0t6bElON1I2UTdyMml4V05GQkMvaldmN05LVWZKeVg4cUlHNW1kMVlVZVQ2R0JXOUJtMi8xL1JpTzI0SlRhWWxmTGRLSzlUWWI4c0c1QitPTGFiMkRJbUc5OUNKMjVSa0FjU29iV05GNXpEME82bGdPbzNjRWRCL2tzQ3EzaG10bEMvRGxMWi9EOENKKzdWdVpuUzFyUjJuYVE9PTwvZHM6WDUwOUNlcnRpZmljYXRlPjwvZHM6WDUwOURhdGE+PC9kczpLZXlJbmZvPjwvZHM6U2lnbmF0dXJlPjxzYW1sOlN1YmplY3Q+PHNhbWw6TmFtZUlEIFNQTmFtZVF1YWxpZmllcj0ic2FtbC1wb2MiIEZvcm1hdD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOm5hbWVpZC1mb3JtYXQ6dHJhbnNpZW50Ij5fYjYxNzFlN2U3ZjE4YTVhMWIyZDVhM2NkOTI3ZDhjYmQ4YTFhOWNlOGMyPC9zYW1sOk5hbWVJRD48c2FtbDpTdWJqZWN0Q29uZmlybWF0aW9uIE1ldGhvZD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmNtOmJlYXJlciI+PHNhbWw6U3ViamVjdENvbmZpcm1hdGlvbkRhdGEgTm90T25PckFmdGVyPSIyMDE5LTExLTAyVDEwOjQyOjM0WiIgUmVjaXBpZW50PSJodHRwOi8vbG9jYWxob3N0OjgwMDAvZ3JvdXAvZ3Vlc3QvcGF5bWVudCIgSW5SZXNwb25zZVRvPSJ6algzR053UDk1ZUNJVVBmYXhhV2VrdERxZXVlIi8+PC9zYW1sOlN1YmplY3RDb25maXJtYXRpb24+PC9zYW1sOlN1YmplY3Q+PHNhbWw6Q29uZGl0aW9ucyBOb3RCZWZvcmU9IjIwMTktMTEtMDJUMTA6Mzc6MDRaIiBOb3RPbk9yQWZ0ZXI9IjIwMTktMTEtMDJUMTA6NDI6MzRaIj48c2FtbDpBdWRpZW5jZVJlc3RyaWN0aW9uPjxzYW1sOkF1ZGllbmNlPnNhbWwtcG9jPC9zYW1sOkF1ZGllbmNlPjwvc2FtbDpBdWRpZW5jZVJlc3RyaWN0aW9uPjwvc2FtbDpDb25kaXRpb25zPjxzYW1sOkF1dGhuU3RhdGVtZW50IEF1dGhuSW5zdGFudD0iMjAxOS0xMS0wMlQwNjoyMToyMFoiIFNlc3Npb25Ob3RPbk9yQWZ0ZXI9IjIwMTktMTEtMDJUMTQ6MjE6MjBaIiBTZXNzaW9uSW5kZXg9Il9hODdjNjcxNThiZGEwMWE0Mjg3NzhhYWY5Mzk4ODYzYWQ5NjJkNzZiMTQiPjxzYW1sOkF1dGhuQ29udGV4dD48c2FtbDpBdXRobkNvbnRleHRDbGFzc1JlZj51cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YWM6Y2xhc3NlczpQYXNzd29yZDwvc2FtbDpBdXRobkNvbnRleHRDbGFzc1JlZj48L3NhbWw6QXV0aG5Db250ZXh0Pjwvc2FtbDpBdXRoblN0YXRlbWVudD48c2FtbDpBdHRyaWJ1dGVTdGF0ZW1lbnQ+PHNhbWw6QXR0cmlidXRlIE5hbWU9InVpZCIgTmFtZUZvcm1hdD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmF0dHJuYW1lLWZvcm1hdDpiYXNpYyI+PHNhbWw6QXR0cmlidXRlVmFsdWUgeHNpOnR5cGU9InhzOnN0cmluZyI+MTwvc2FtbDpBdHRyaWJ1dGVWYWx1ZT48L3NhbWw6QXR0cmlidXRlPjxzYW1sOkF0dHJpYnV0ZSBOYW1lPSJlZHVQZXJzb25BZmZpbGlhdGlvbiIgTmFtZUZvcm1hdD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmF0dHJuYW1lLWZvcm1hdDpiYXNpYyI+PHNhbWw6QXR0cmlidXRlVmFsdWUgeHNpOnR5cGU9InhzOnN0cmluZyI+Z3JvdXAxPC9zYW1sOkF0dHJpYnV0ZVZhbHVlPjwvc2FtbDpBdHRyaWJ1dGU+PHNhbWw6QXR0cmlidXRlIE5hbWU9ImVtYWlsIiBOYW1lRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YXR0cm5hbWUtZm9ybWF0OmJhc2ljIj48c2FtbDpBdHRyaWJ1dGVWYWx1ZSB4c2k6dHlwZT0ieHM6c3RyaW5nIj51c2VyMUBleGFtcGxlLmNvbTwvc2FtbDpBdHRyaWJ1dGVWYWx1ZT48L3NhbWw6QXR0cmlidXRlPjwvc2FtbDpBdHRyaWJ1dGVTdGF0ZW1lbnQ+PC9zYW1sOkFzc2VydGlvbj48L3NhbWxwOlJlc3BvbnNlPg==";
    // XmlValidator.decode_decompress(string);



    String[] credentials = null;
    _log.info("---------------------------------------------");
    _log.info("##########     AUTHOR : DARSHH     ##########");
    _log.info("---------------------------------------------");
    try {

      // AssertionLocalServiceUtil.cleanDeprecatedAssertion(new Date());

      long companyId = PortalUtil.getCompanyId(request);

      if (!PrefsPropsUtil.getBoolean(companyId, PropsKeys.SAML_AUTH_ENABLED,
          PropsValues.SAML_AUTH_ENABLED)) {
        return credentials;
      }

      String loginUrl = SAMLMetadataManager.getIDPSingleSignOnLocation();
      _log.info("loginURL-> " + loginUrl);
      String samlEntityId =
          PrefsPropsUtil.getString(companyId, PropsKeys.SAML_ENTITY_ID, PropsValues.SAML_ENTITY_ID);
      _log.info("samlEntityId-> " + samlEntityId);
      String samlIdentityProvider = SAMLMetadataManager.getIDPEntityID();
      _log.info("samlIdentityProvider-> " + samlIdentityProvider);
      String signAlg = PrefsPropsUtil.getString(companyId, PropsKeys.SAML_REDIRECT_SIGN_ALG,
          PropsValues.SAML_REDIRECT_SIGN_ALG);
      _log.info("signAlg-> " + signAlg);
      if (!SAMLUtil.isAuthenticated(request, loginUrl, samlEntityId, samlIdentityProvider, signAlg,
          response)) {
        _log.info("Not Authenticated");
        return credentials;
      }
      _log.info("Authenticated");


      String emailAddressAttr = PrefsPropsUtil.getString(companyId,
          PropsKeys.SAML_EMAIL_ADDRESS_ATTR, PropsValues.SAML_EMAIL_ADDRESS_ATTR);
      // String screenNameAttr = PrefsPropsUtil.getString(companyId,
      // PropsKeys.SAML_SCREEN_NAME_ATTR,
      // PropsValues.SAML_SCREEN_NAME_ATTR);
      // String firstNameAttr = PrefsPropsUtil.getString(companyId, PropsKeys.SAML_FIRST_NAME_ATTR,
      // PropsValues.SAML_FIRST_NAME_ATTR);
      // String lastNameAttr = PrefsPropsUtil.getString(companyId, PropsKeys.SAML_LAST_NAME_ATTR,
      // PropsValues.SAML_LAST_NAME_ATTR);
      boolean ldapImportEnabled = PrefsPropsUtil.getBoolean(companyId,
          PropsKeys.SAML_LDAP_IMPORT_ENABLED, PropsValues.SAML_LDAP_IMPORT_ENABLED);


      // String birthDayAttr = PrefsPropsUtil.getString(companyId, PropsKeys.SAML_BIRTH_DAY_ATTR,
      // PropsValues.SAML_BIRTH_DAY_ATTR);
      // String birthDayFormat = PrefsPropsUtil.getString(companyId,
      // PropsKeys.SAML_BIRTH_DAY_FORMAT,
      // PropsValues.SAML_BIRTH_DAY_FORMAT);
      //
      // String[] phoneAttrs = PrefsPropsUtil.getStringArray(companyId, PropsKeys.SAML_PHONE_ATTRS,
      // StringPool.COMMA, PropsValues.SAML_PHONE_ATTRS);
      // int[] phoneTypesId = GetterUtil.getIntegerValues(PrefsPropsUtil.getStringArray(companyId,
      // PropsKeys.SAML_PHONE_TYPES_ID, StringPool.COMMA, PropsValues.SAML_PHONE_TYPES_ID));
      //
      // String[] streetAttrs =
      // PrefsPropsUtil.getStringArray(companyId, PropsKeys.SAML_ADDRESS_STREET_ATTRS,
      // StringPool.COMMA, PropsValues.SAML_ADDRESS_STREET_ATTRS);
      // String[] postalcodeAttrs =
      // PrefsPropsUtil.getStringArray(companyId, PropsKeys.SAML_ADDRESS_POSTALCODE_ATTRS,
      // StringPool.COMMA, PropsValues.SAML_ADDRESS_POSTALCODE_ATTRS);
      // String[] cityAttrs = PrefsPropsUtil.getStringArray(companyId,
      // PropsKeys.SAML_ADDRESS_CITY_ATTRS, StringPool.COMMA, PropsValues.SAML_ADDRESS_CITY_ATTRS);
      // String[] countryAttrs =
      // PrefsPropsUtil.getStringArray(companyId, PropsKeys.SAML_ADDRESS_COUNTRY_ATTRS,
      // StringPool.COMMA, PropsValues.SAML_ADDRESS_COUNTRY_ATTRS);
      // long defaultCountryId = PrefsPropsUtil.getLong(companyId,
      // PropsKeys.SAML_ADDRESS_DEFAULT_COUNTRY_ID, PropsValues.SAML_ADDRESS_DEFAULT_COUNTRY_ID);
      // String[] regionAttrs =
      // PrefsPropsUtil.getStringArray(companyId, PropsKeys.SAML_ADDRESS_REGION_ATTRS,
      // StringPool.COMMA, PropsValues.SAML_ADDRESS_REGION_ATTRS);
      // int[] addressTypesId = GetterUtil.getIntegerValues(PrefsPropsUtil.getStringArray(companyId,
      // PropsKeys.SAML_ADDRESS_TYPES_ID, StringPool.COMMA, PropsValues.SAML_ADDRESS_TYPES_ID));

      _log.info("samlEntityId  " + samlEntityId);
      _log.info("signAlg  " + signAlg);
      _log.info("emailAddressAttr " + emailAddressAttr);

      Map<String, String> nameValues = SAMLUtil.getAttributes(request);

      // String screenName = nameValues.get(screenNameAttr);
      String emailAddress = nameValues.get(emailAddressAttr);
      // String firstName = nameValues.get(firstNameAttr);
      // String lastName = nameValues.get(lastNameAttr);

      _log.info("emailAddress -> " + emailAddress);

      Date birthDay = new Date();
      // String birthDayStr = nameValues.get(birthDayAttr);
      // if (Validator.isNotNull(birthDayStr)) {
      // try {
      // birthDay = new SimpleDateFormat(birthDayFormat).parse(birthDayStr);
      // } catch (ParseException pe) {
      // _log.error(pe, pe);
      // }
      // }

      // if (_log.isDebugEnabled()) {
      // _log.debug(new StringBundler(8).append("Validating user information for").append(firstName)
      // .append(StringPool.BLANK).append(lastName).append(" with screen name ")
      // .append(screenName).append(" and email address ").append(emailAddress).toString());
      // }

      User user = null;

      if (ldapImportEnabled) {
        // user = PortalLDAPImporterUtil.importLDAPUserByScreenName(
        // companyId, screenName);
      } else {
        if (Validator.isNull(emailAddress)) {
          if (_log.isWarnEnabled()) {
            _log.warn("Email address is null");
          }

          emailAddress = StringPool.BLANK;
        }
        UserLocalServiceUtil.getUserByEmailAddress(20099, "ga1@gmail.com");
        try {
          // user = UserLocalServiceUtil.getUserByScreenName(companyId, screenName);
          user = UserLocalServiceUtil.getUserByEmailAddress(companyId, emailAddress);
        } catch (NoSuchUserException nsue) {
          _log.error("--- Exception --- " + nsue);
        }
      }

      if (user == null) {
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

        Locale locale = LocaleUtil.getDefault();

        if (themeDisplay != null) {

          // ThemeDisplay should never be null, but some users
          // complain of this error. Cause is unknown.

          locale = themeDisplay.getLocale();
        }

        // if (_log.isDebugEnabled()) {
        // _log.debug("Adding user " + screenName);
        // }

        // user = addUser(companyId, firstName, lastName, emailAddress, screenName, birthDay,
        // locale);
      } else {
        if (Validator.isNotNull(emailAddress)) {
          user.setEmailAddress(emailAddress);
        }

        // if (Validator.isNotNull(firstName)) {
        // user.setFirstName(firstName);
        // }
        //
        // if (Validator.isNotNull(lastName)) {
        // user.setLastName(lastName);
        // }

        if (Validator.isNotNull(birthDay)) {
          Contact contact = user.getContact();
          contact.setBirthday(birthDay);
          ContactLocalServiceUtil.updateContact(contact);
        }

        UserLocalServiceUtil.updateUser(user);
      }

      // long userId = user.getUserId();
      // String className = Contact.class.getName();
      // long contactId = user.getContactId();

      // List<Phone> phones = PhoneLocalServiceUtil.getPhones(companyId,
      // className, contactId);
      //
      // for (int i = 0; i < phoneTypesId.length; ++i) {
      // String phoneNumber = nameValues.get(phoneAttrs[i]);
      // int typeId = phoneTypesId[i];
      //
      // if (Validator.isNotNull(phoneNumber)) {
      // boolean notFound = true;
      //
      // if (phones != null) {
      // for (Phone phone : phones) {
      // if (phone.getTypeId() == typeId) {
      // notFound = false;
      // phone.setNumber(phoneNumber);
      //
      // PhoneLocalServiceUtil.updatePhone(phone);
      // break;
      // }
      // }
      // }
      //
      // if (notFound) {
      // // PhoneLocalServiceUtil.addPhone(userId, className, contactId,
      // phoneNumber,
      // // StringPool.BLANK, typeId, false);
      // }
      // }
      // }
      //
      // List<Address> addresses =
      // AddressLocalServiceUtil.getAddresses(companyId, className,
      // contactId);

      // for (int i = 0; i < addressTypesId.length; ++i) {
      // try {
      // String street = nameValues.get(streetAttrs[i]);
      // String postalCode = nameValues.get(postalcodeAttrs[i]);
      // String city = nameValues.get(cityAttrs[i]);
      // String country = nameValues.get(countryAttrs[i]);
      // String region = nameValues.get(regionAttrs[i]);
      // int typeId = addressTypesId[i];
      //
      // long countryId = defaultCountryId;
      // if (Validator.isNotNull(country)) {
      // Country countryByName =
      // CountryServiceUtil.getCountryByName(country);
      //
      // if (countryByName != null) {
      // countryId = countryByName.getCountryId();
      // }
      // }
      //
      // long regionId = 0;
      // if (Validator.isNotNull(region)) {
      // List<Region> regions = RegionServiceUtil.getRegions(countryId);
      // for (Region regionByName : regions) {
      // if (regionByName.getName().equalsIgnoreCase(region)) {
      // regionId = regionByName.getRegionId();
      // break;
      // }
      // }
      // }
      //
      // if (Validator.isNotNull(street) ||
      // Validator.isNotNull(postalCode)
      // || Validator.isNotNull(city) || Validator.isNotNull(country)
      // || Validator.isNotNull(region)) {
      //
      // boolean notFound = true;
      //
      // if (addresses != null) {
      // for (Address address : addresses) {
      // if (address.getTypeId() == typeId) {
      // notFound = false;
      // address.setStreet1(street);
      // address.setZip(postalCode);
      // address.setCity(city);
      // address.setCountryId(countryId);
      // address.setRegionId(regionId);
      //
      // AddressLocalServiceUtil.updateAddress(address);
      // break;
      // }
      // }
      // }
      //
      // if (notFound) {
      // // AddressLocalServiceUtil.addAddress(userId, className,
      // contactId, street,
      // // StringPool.BLANK, StringPool.BLANK, city, postalCode,
      // regionId, countryId, typeId,
      // // false, false);
      // }
      // }
      // } catch (SystemException se) {
      // _log.warn("error with address", se);
      // }
      // }

      credentials = new String[3];
      credentials[0] = String.valueOf(user.getUserId());
      credentials[1] = user.getPassword();
      credentials[2] = Boolean.TRUE.toString();
    } catch (Exception e) {
      _log.error(e, e);
    }

    return credentials;
  }

  protected User addUser(long companyId, String firstName, String lastName, String emailAddress,
      String screenName, Date birthDay, Locale locale) throws Exception {

    long creatorUserId = 0;
    boolean autoPassword = false;
    String password1 = PwdGenerator.getPassword();
    String password2 = password1;
    boolean autoScreenName = false;
    long facebookId = 0;
    String openId = StringPool.BLANK;
    String middleName = StringPool.BLANK;
    int prefixId = 0;
    int suffixId = 0;
    boolean male = true;

    int birthdayMonth = Calendar.JANUARY;
    int birthdayDay = 1;
    int birthdayYear = 1970;
    if (birthDay != null) {
      Calendar birthDayCalendar = Calendar.getInstance();
      birthDayCalendar.setTime(birthDay);
      birthdayMonth = birthDayCalendar.get(Calendar.MONTH);
      birthdayDay = birthDayCalendar.get(Calendar.DAY_OF_MONTH);
      birthdayYear = birthDayCalendar.get(Calendar.YEAR);
    }

    String jobTitle = StringPool.BLANK;
    long[] groupIds = null;
    long[] organizationIds = null;
    long[] roleIds = null;
    long[] userGroupIds = null;
    boolean sendEmail = false;
    ServiceContext serviceContext = new ServiceContext();

    return UserLocalServiceUtil.addUser(creatorUserId, companyId, autoPassword, password1,
        password2, autoScreenName, screenName, emailAddress, facebookId, openId, locale, firstName,
        middleName, lastName, prefixId, suffixId, male, birthdayMonth, birthdayDay, birthdayYear,
        jobTitle, groupIds, organizationIds, roleIds, userGroupIds, sendEmail, serviceContext);
  }

  // // encoding url
  // public static String encode(String url) {
  // try {
  // String encodeURL = URLEncoder.encode(url, "UTF-8");
  // return encodeURL;
  // } catch (UnsupportedEncodingException e) {
  // return "Issue while encoding" e.getMessage();
  // }
  // }

  // http://localhost:8080/c/portal/login?parameterAutoLoginLogin=ga1@gmail.com&parameterAutoLoginPassword=AAAAoAAB9ABl9%2Bu6ajn4iCjfFZzxAAGnJ9gAqXxKV5WeUonE
  // http://localhost:8000/c/portal/login/callback

  @Override
  public String[] handleException(HttpServletRequest request, HttpServletResponse response,
      Exception e) throws AutoLoginException {
    // TODO Auto-generated method stub
    return null;
  }

  public static String getHTTPRedirectBindingUrl(String serviceUrl, boolean isRequest, String saml,
      String relayState, String signAlg)
      throws SystemException, IOException, GeneralSecurityException {

    byte[] samlDeflate = Deflate.encode(saml);

    String samlBase64 = Base64.encode(samlDeflate);

    StringBundler url = new StringBundler(7);
    url.append(serviceUrl).append(StringPool.QUESTION);

    StringBundler urlParamBuffer = new StringBundler(11);
    if (isRequest) {
      urlParamBuffer.append("SAMLRequest");
    } else {
      urlParamBuffer.append("SAMLResponse");
    }
    urlParamBuffer.append(StringPool.EQUAL).append(HttpUtil.encodeURL(samlBase64));

    if (Validator.isNotNull(relayState)) {
      urlParamBuffer.append(StringPool.AMPERSAND).append("RelayState").append(StringPool.EQUAL)
          .append(HttpUtil.encodeURL(relayState));
    }

    urlParamBuffer.append(StringPool.AMPERSAND).append("SigAlg").append(StringPool.EQUAL)
        .append(HttpUtil.encodeURL(_getURIFromSignAlg(signAlg)));
    String urlParameters = urlParamBuffer.toString();

    url.append(urlParameters);

    url.append(StringPool.AMPERSAND).append("Signature").append(StringPool.EQUAL)
        .append(_signUrlParameter(urlParameters, signAlg));

    return url.toString();
  }

  public static void initHTTPHeader(HttpServletResponse response) {
    response.setHeader("Cache-Control", "no-cache, no-store");
    response.setHeader("Pragma", "no-cache");
  }

  private static String _signUrlParameter(String urlParameter, String signAlg)
      throws SystemException, IOException, GeneralSecurityException {

    Signature signature = Signature.getInstance(_getAlgorithmFromSignAlg(signAlg));

    KeyStore keystore = KeyStore.getInstance("jks");
    // keystore.load(new FileInputStream("chemin/vers/monkeystore.jks"),
    // "MotDePasseDuKeyStore".toCharArray());
    // keystore.load(new FileInputStream("chemin/vers/yourkeystore.jks"),
    // "password".toCharArray());
    keystore.load(
        new FileInputStream(
            "/home/aspire101/Projects/SAML/ga1/liferay-ce-portal-7.1.0-ga1/tomcat-9.0.6/conf/monkeystore.jks"),
        "password".toCharArray());

    // PrivateKey spPrivateKey =
    // (PrivateKey) keystore.getKey("aliasDeLaClePrivee",
    // "MotDePasseDeLaClePrivee".toCharArray());
    PrivateKey spPrivateKey = (PrivateKey) keystore.getKey("oauthAlias", "password".toCharArray());

    signature.initSign(SAMLMetadataManager.getSPPrivateKey());
    signature.initSign(spPrivateKey);

    signature.update(urlParameter.getBytes(StringPool.UTF8));
    return HttpUtil.encodeURL(Base64.encode(signature.sign()));
  }

  // private static String _getURIFromSignAlg(String signAlg) {
  // if (_SHA256_WITH_RSA.equals(signAlg)) {
  // return "http://www.w3.org/2000/09/xmldsig#rsa-sha256";
  // // return SignatureMethod.DSA_SHA1;
  // }
  // return SignatureMethod.RSA_SHA1;
  // }
  //
  // private static String _getAlgorithmFromSignAlg(String signAlg) {
  // if (_SHA256_WITH_RSA.equals(signAlg)) {
  // return _SHA256_WITH_RSA;
  // }
  // return _SHA1_WITH_RSA;
  // }
  private static String _getURIFromSignAlg(String signAlg) {
    if (_DSA_WITH_SHA1.equals(signAlg)) {
      return SignatureMethod.DSA_SHA1;
    }
    return SignatureMethod.RSA_SHA1;
  }

  private static String _getAlgorithmFromSignAlg(String signAlg) {
    if (_DSA_WITH_SHA1.equals(signAlg)) {
      return _SHA1_WITH_DSA;
    }
    return _SHA1_WITH_RSA;
  }

  private static final String _DSA_WITH_SHA1 = "DSAwithSHA1";

  private static final String _SHA1_WITH_DSA = "SHA1withDSA";

  private static final String _SHA1_WITH_RSA = "SHA1withRSA";
  private static final String _SHA256_WITH_RSA = "RSAwithSHA256";
  // private static final String _SHA1_WITH_RSA = "rsa-sha1";

  public static String generateAuthnRequest(HttpServletRequest request, String loginUrl,
      String samlEntityId, String relayState, String signAlg, Boolean isPassive) throws Exception {

    String messageID = generateMessageID();

    request.getSession().setAttribute("messageID", messageID);

    Document document = SAXReaderUtil.createDocument();

    Namespace samlp = SAXReaderUtil.createNamespace("SAMLP", SAMLConstants.PROTOCOL_NAMESPACE);
    Namespace saml = SAXReaderUtil.createNamespace("SAML", SAMLConstants.ASSERTION_NAMESPACE);

    Element authnRequest =
        document.addElement(SAXReaderUtil.createQName(SAMLConstants.AUTHN_REQUEST, samlp));

    authnRequest.add(samlp);
    authnRequest.add(saml);

    authnRequest.addAttribute(SAMLConstants.ID, messageID);
    authnRequest.addAttribute(SAMLConstants.VERSION, SAMLConstants.VERSION_2_0);
    authnRequest.addAttribute(SAMLConstants.ISSUE_INSTANT, formatDate(new Date()));
    authnRequest.addAttribute(SAMLConstants.DESTINATION, loginUrl);

    if (isPassive != null) {
      authnRequest.addAttribute(SAMLConstants.IS_PASSIVE, isPassive.toString());
    }

    Element issuer = authnRequest.addElement(SAXReaderUtil.createQName(SAMLConstants.ISSUER, saml));
    issuer.setText(samlEntityId);

    String xmlString = document.asXML();
    if (_log.isDebugEnabled()) {
      _log.debug("send : " + xmlString);
    }

    return getHTTPRedirectBindingUrl(loginUrl, true, xmlString, relayState, signAlg);
  }

  public static String generateMessageID() {
    // the id must be unique, in the case of a random
    // technique , the standard recommends a length of more than
    // 160 bit (less than 2 ^ -160 collision chance) )
    // the base 64 encoding extension of 1/3, in full bytes, the top length
    // the most
    // approaching is 21 (21 * 8 = 168> 160), so we will output an id of 28
    // characters.
    byte[] randomBytes = new byte[21];
    _random.nextBytes(randomBytes);
    return Base64.encode(randomBytes);
  }

  public static String formatDate(Date date) {
    return _getDateFormat().format(date);
  }

  private static DateFormat _getDateFormat() {
    return new SimpleDateFormat(_DATE_PATTERN);
  }

  private static final String _DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'Z'";

  private static Random _random = new Random();

  // public static boolean isAuthenticated(HttpServletRequest request, String
  // loginUrl,
  // String samlEntityId, String samlIdentityProviderId, String signAlg,
  // HttpServletResponse response) throws Exception {
  //
  // String samlResponseParameter =
  // request.getParameter(SAMLConstants.SAML_RESPONSE);
  // SAMLServiceProvider serviceProvider =
  // SAMLMetadataManager.getServiceProvider();
  // if (samlResponseParameter != null) {
  // String samlResponse =
  // SAMLUtil.getSamlFromHTTPPostBindingParameter(samlResponseParameter);
  // return serviceProvider.receiveResponse(request, samlEntityId,
  // samlIdentityProviderId,
  // samlResponse);
  // } else {
  // serviceProvider.sendAuthnRequest(request, response, samlEntityId,
  // loginUrl, signAlg);
  // }
  // return false;
  // }

}
