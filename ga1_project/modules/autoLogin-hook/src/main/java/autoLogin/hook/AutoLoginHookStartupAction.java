package autoLogin.hook;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.SimpleAction;

/***
 * 
 * @author aspire101
 */
public class AutoLoginHookStartupAction extends SimpleAction {

  @Override
  public void run(String[] lifecycleEventIds) throws ActionException {
    for (String eventId : lifecycleEventIds) {
      System.out.println("Startup event ID " + eventId);
    }
  }

}
