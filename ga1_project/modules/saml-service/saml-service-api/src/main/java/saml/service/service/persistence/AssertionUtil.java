/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package saml.service.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

import saml.service.model.Assertion;

import java.util.List;

/**
 * The persistence utility for the assertion service. This utility wraps {@link saml.service.service.persistence.impl.AssertionPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AssertionPersistence
 * @see saml.service.service.persistence.impl.AssertionPersistenceImpl
 * @generated
 */
@ProviderType
public class AssertionUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Assertion assertion) {
		getPersistence().clearCache(assertion);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Assertion> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Assertion> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Assertion> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Assertion> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Assertion update(Assertion assertion) {
		return getPersistence().update(assertion);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Assertion update(Assertion assertion,
		ServiceContext serviceContext) {
		return getPersistence().update(assertion, serviceContext);
	}

	/**
	* Returns all the assertions where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching assertions
	*/
	public static List<Assertion> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the assertions where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of assertions
	* @param end the upper bound of the range of assertions (not inclusive)
	* @return the range of matching assertions
	*/
	public static List<Assertion> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the assertions where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of assertions
	* @param end the upper bound of the range of assertions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assertions
	*/
	public static List<Assertion> findByUuid(String uuid, int start, int end,
		OrderByComparator<Assertion> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the assertions where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of assertions
	* @param end the upper bound of the range of assertions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching assertions
	*/
	public static List<Assertion> findByUuid(String uuid, int start, int end,
		OrderByComparator<Assertion> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first assertion in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assertion
	* @throws NoSuchAssertionException if a matching assertion could not be found
	*/
	public static Assertion findByUuid_First(String uuid,
		OrderByComparator<Assertion> orderByComparator)
		throws saml.service.exception.NoSuchAssertionException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first assertion in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assertion, or <code>null</code> if a matching assertion could not be found
	*/
	public static Assertion fetchByUuid_First(String uuid,
		OrderByComparator<Assertion> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last assertion in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assertion
	* @throws NoSuchAssertionException if a matching assertion could not be found
	*/
	public static Assertion findByUuid_Last(String uuid,
		OrderByComparator<Assertion> orderByComparator)
		throws saml.service.exception.NoSuchAssertionException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last assertion in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assertion, or <code>null</code> if a matching assertion could not be found
	*/
	public static Assertion fetchByUuid_Last(String uuid,
		OrderByComparator<Assertion> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the assertions before and after the current assertion in the ordered set where uuid = &#63;.
	*
	* @param assertionId the primary key of the current assertion
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assertion
	* @throws NoSuchAssertionException if a assertion with the primary key could not be found
	*/
	public static Assertion[] findByUuid_PrevAndNext(long assertionId,
		String uuid, OrderByComparator<Assertion> orderByComparator)
		throws saml.service.exception.NoSuchAssertionException {
		return getPersistence()
				   .findByUuid_PrevAndNext(assertionId, uuid, orderByComparator);
	}

	/**
	* Removes all the assertions where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of assertions where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching assertions
	*/
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the assertion where generatedKey = &#63; or throws a {@link NoSuchAssertionException} if it could not be found.
	*
	* @param generatedKey the generated key
	* @return the matching assertion
	* @throws NoSuchAssertionException if a matching assertion could not be found
	*/
	public static Assertion findByGeneratedKey(String generatedKey)
		throws saml.service.exception.NoSuchAssertionException {
		return getPersistence().findByGeneratedKey(generatedKey);
	}

	/**
	* Returns the assertion where generatedKey = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param generatedKey the generated key
	* @return the matching assertion, or <code>null</code> if a matching assertion could not be found
	*/
	public static Assertion fetchByGeneratedKey(String generatedKey) {
		return getPersistence().fetchByGeneratedKey(generatedKey);
	}

	/**
	* Returns the assertion where generatedKey = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param generatedKey the generated key
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching assertion, or <code>null</code> if a matching assertion could not be found
	*/
	public static Assertion fetchByGeneratedKey(String generatedKey,
		boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByGeneratedKey(generatedKey, retrieveFromCache);
	}

	/**
	* Removes the assertion where generatedKey = &#63; from the database.
	*
	* @param generatedKey the generated key
	* @return the assertion that was removed
	*/
	public static Assertion removeByGeneratedKey(String generatedKey)
		throws saml.service.exception.NoSuchAssertionException {
		return getPersistence().removeByGeneratedKey(generatedKey);
	}

	/**
	* Returns the number of assertions where generatedKey = &#63;.
	*
	* @param generatedKey the generated key
	* @return the number of matching assertions
	*/
	public static int countByGeneratedKey(String generatedKey) {
		return getPersistence().countByGeneratedKey(generatedKey);
	}

	/**
	* Caches the assertion in the entity cache if it is enabled.
	*
	* @param assertion the assertion
	*/
	public static void cacheResult(Assertion assertion) {
		getPersistence().cacheResult(assertion);
	}

	/**
	* Caches the assertions in the entity cache if it is enabled.
	*
	* @param assertions the assertions
	*/
	public static void cacheResult(List<Assertion> assertions) {
		getPersistence().cacheResult(assertions);
	}

	/**
	* Creates a new assertion with the primary key. Does not add the assertion to the database.
	*
	* @param assertionId the primary key for the new assertion
	* @return the new assertion
	*/
	public static Assertion create(long assertionId) {
		return getPersistence().create(assertionId);
	}

	/**
	* Removes the assertion with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param assertionId the primary key of the assertion
	* @return the assertion that was removed
	* @throws NoSuchAssertionException if a assertion with the primary key could not be found
	*/
	public static Assertion remove(long assertionId)
		throws saml.service.exception.NoSuchAssertionException {
		return getPersistence().remove(assertionId);
	}

	public static Assertion updateImpl(Assertion assertion) {
		return getPersistence().updateImpl(assertion);
	}

	/**
	* Returns the assertion with the primary key or throws a {@link NoSuchAssertionException} if it could not be found.
	*
	* @param assertionId the primary key of the assertion
	* @return the assertion
	* @throws NoSuchAssertionException if a assertion with the primary key could not be found
	*/
	public static Assertion findByPrimaryKey(long assertionId)
		throws saml.service.exception.NoSuchAssertionException {
		return getPersistence().findByPrimaryKey(assertionId);
	}

	/**
	* Returns the assertion with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param assertionId the primary key of the assertion
	* @return the assertion, or <code>null</code> if a assertion with the primary key could not be found
	*/
	public static Assertion fetchByPrimaryKey(long assertionId) {
		return getPersistence().fetchByPrimaryKey(assertionId);
	}

	public static java.util.Map<java.io.Serializable, Assertion> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the assertions.
	*
	* @return the assertions
	*/
	public static List<Assertion> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the assertions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of assertions
	* @param end the upper bound of the range of assertions (not inclusive)
	* @return the range of assertions
	*/
	public static List<Assertion> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the assertions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of assertions
	* @param end the upper bound of the range of assertions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of assertions
	*/
	public static List<Assertion> findAll(int start, int end,
		OrderByComparator<Assertion> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the assertions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of assertions
	* @param end the upper bound of the range of assertions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of assertions
	*/
	public static List<Assertion> findAll(int start, int end,
		OrderByComparator<Assertion> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the assertions from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of assertions.
	*
	* @return the number of assertions
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static AssertionPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<AssertionPersistence, AssertionPersistence> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(AssertionPersistence.class);

		ServiceTracker<AssertionPersistence, AssertionPersistence> serviceTracker =
			new ServiceTracker<AssertionPersistence, AssertionPersistence>(bundle.getBundleContext(),
				AssertionPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}