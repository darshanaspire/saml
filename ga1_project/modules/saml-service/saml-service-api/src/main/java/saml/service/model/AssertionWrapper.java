/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package saml.service.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Assertion}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Assertion
 * @generated
 */
@ProviderType
public class AssertionWrapper implements Assertion, ModelWrapper<Assertion> {
	public AssertionWrapper(Assertion assertion) {
		_assertion = assertion;
	}

	@Override
	public Class<?> getModelClass() {
		return Assertion.class;
	}

	@Override
	public String getModelClassName() {
		return Assertion.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("assertionId", getAssertionId());
		attributes.put("generatedKey", getGeneratedKey());
		attributes.put("notOnOrAfter", getNotOnOrAfter());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long assertionId = (Long)attributes.get("assertionId");

		if (assertionId != null) {
			setAssertionId(assertionId);
		}

		String generatedKey = (String)attributes.get("generatedKey");

		if (generatedKey != null) {
			setGeneratedKey(generatedKey);
		}

		Date notOnOrAfter = (Date)attributes.get("notOnOrAfter");

		if (notOnOrAfter != null) {
			setNotOnOrAfter(notOnOrAfter);
		}
	}

	@Override
	public Object clone() {
		return new AssertionWrapper((Assertion)_assertion.clone());
	}

	@Override
	public int compareTo(Assertion assertion) {
		return _assertion.compareTo(assertion);
	}

	/**
	* Returns the assertion ID of this assertion.
	*
	* @return the assertion ID of this assertion
	*/
	@Override
	public long getAssertionId() {
		return _assertion.getAssertionId();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _assertion.getExpandoBridge();
	}

	/**
	* Returns the generated key of this assertion.
	*
	* @return the generated key of this assertion
	*/
	@Override
	public String getGeneratedKey() {
		return _assertion.getGeneratedKey();
	}

	/**
	* Returns the not on or after of this assertion.
	*
	* @return the not on or after of this assertion
	*/
	@Override
	public Date getNotOnOrAfter() {
		return _assertion.getNotOnOrAfter();
	}

	/**
	* Returns the primary key of this assertion.
	*
	* @return the primary key of this assertion
	*/
	@Override
	public long getPrimaryKey() {
		return _assertion.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _assertion.getPrimaryKeyObj();
	}

	/**
	* Returns the uuid of this assertion.
	*
	* @return the uuid of this assertion
	*/
	@Override
	public String getUuid() {
		return _assertion.getUuid();
	}

	@Override
	public int hashCode() {
		return _assertion.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _assertion.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _assertion.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _assertion.isNew();
	}

	@Override
	public void persist() {
		_assertion.persist();
	}

	/**
	* Sets the assertion ID of this assertion.
	*
	* @param assertionId the assertion ID of this assertion
	*/
	@Override
	public void setAssertionId(long assertionId) {
		_assertion.setAssertionId(assertionId);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_assertion.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_assertion.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_assertion.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_assertion.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the generated key of this assertion.
	*
	* @param generatedKey the generated key of this assertion
	*/
	@Override
	public void setGeneratedKey(String generatedKey) {
		_assertion.setGeneratedKey(generatedKey);
	}

	@Override
	public void setNew(boolean n) {
		_assertion.setNew(n);
	}

	/**
	* Sets the not on or after of this assertion.
	*
	* @param notOnOrAfter the not on or after of this assertion
	*/
	@Override
	public void setNotOnOrAfter(Date notOnOrAfter) {
		_assertion.setNotOnOrAfter(notOnOrAfter);
	}

	/**
	* Sets the primary key of this assertion.
	*
	* @param primaryKey the primary key of this assertion
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_assertion.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_assertion.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the uuid of this assertion.
	*
	* @param uuid the uuid of this assertion
	*/
	@Override
	public void setUuid(String uuid) {
		_assertion.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Assertion> toCacheModel() {
		return _assertion.toCacheModel();
	}

	@Override
	public Assertion toEscapedModel() {
		return new AssertionWrapper(_assertion.toEscapedModel());
	}

	@Override
	public String toString() {
		return _assertion.toString();
	}

	@Override
	public Assertion toUnescapedModel() {
		return new AssertionWrapper(_assertion.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _assertion.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AssertionWrapper)) {
			return false;
		}

		AssertionWrapper assertionWrapper = (AssertionWrapper)obj;

		if (Objects.equals(_assertion, assertionWrapper._assertion)) {
			return true;
		}

		return false;
	}

	@Override
	public Assertion getWrappedModel() {
		return _assertion;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _assertion.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _assertion.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_assertion.resetOriginalValues();
	}

	private final Assertion _assertion;
}