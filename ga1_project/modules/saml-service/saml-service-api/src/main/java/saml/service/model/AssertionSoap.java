/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package saml.service.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link saml.service.service.http.AssertionServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see saml.service.service.http.AssertionServiceSoap
 * @generated
 */
@ProviderType
public class AssertionSoap implements Serializable {
	public static AssertionSoap toSoapModel(Assertion model) {
		AssertionSoap soapModel = new AssertionSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setAssertionId(model.getAssertionId());
		soapModel.setGeneratedKey(model.getGeneratedKey());
		soapModel.setNotOnOrAfter(model.getNotOnOrAfter());

		return soapModel;
	}

	public static AssertionSoap[] toSoapModels(Assertion[] models) {
		AssertionSoap[] soapModels = new AssertionSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AssertionSoap[][] toSoapModels(Assertion[][] models) {
		AssertionSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AssertionSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AssertionSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AssertionSoap[] toSoapModels(List<Assertion> models) {
		List<AssertionSoap> soapModels = new ArrayList<AssertionSoap>(models.size());

		for (Assertion model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AssertionSoap[soapModels.size()]);
	}

	public AssertionSoap() {
	}

	public long getPrimaryKey() {
		return _assertionId;
	}

	public void setPrimaryKey(long pk) {
		setAssertionId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getAssertionId() {
		return _assertionId;
	}

	public void setAssertionId(long assertionId) {
		_assertionId = assertionId;
	}

	public String getGeneratedKey() {
		return _generatedKey;
	}

	public void setGeneratedKey(String generatedKey) {
		_generatedKey = generatedKey;
	}

	public Date getNotOnOrAfter() {
		return _notOnOrAfter;
	}

	public void setNotOnOrAfter(Date notOnOrAfter) {
		_notOnOrAfter = notOnOrAfter;
	}

	private String _uuid;
	private long _assertionId;
	private String _generatedKey;
	private Date _notOnOrAfter;
}