/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package saml.service.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import saml.service.exception.NoSuchAssertionException;

import saml.service.model.Assertion;

/**
 * The persistence interface for the assertion service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see saml.service.service.persistence.impl.AssertionPersistenceImpl
 * @see AssertionUtil
 * @generated
 */
@ProviderType
public interface AssertionPersistence extends BasePersistence<Assertion> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AssertionUtil} to access the assertion persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the assertions where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching assertions
	*/
	public java.util.List<Assertion> findByUuid(String uuid);

	/**
	* Returns a range of all the assertions where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of assertions
	* @param end the upper bound of the range of assertions (not inclusive)
	* @return the range of matching assertions
	*/
	public java.util.List<Assertion> findByUuid(String uuid, int start, int end);

	/**
	* Returns an ordered range of all the assertions where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of assertions
	* @param end the upper bound of the range of assertions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching assertions
	*/
	public java.util.List<Assertion> findByUuid(String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Assertion> orderByComparator);

	/**
	* Returns an ordered range of all the assertions where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of assertions
	* @param end the upper bound of the range of assertions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching assertions
	*/
	public java.util.List<Assertion> findByUuid(String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Assertion> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first assertion in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assertion
	* @throws NoSuchAssertionException if a matching assertion could not be found
	*/
	public Assertion findByUuid_First(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Assertion> orderByComparator)
		throws NoSuchAssertionException;

	/**
	* Returns the first assertion in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching assertion, or <code>null</code> if a matching assertion could not be found
	*/
	public Assertion fetchByUuid_First(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Assertion> orderByComparator);

	/**
	* Returns the last assertion in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assertion
	* @throws NoSuchAssertionException if a matching assertion could not be found
	*/
	public Assertion findByUuid_Last(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Assertion> orderByComparator)
		throws NoSuchAssertionException;

	/**
	* Returns the last assertion in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching assertion, or <code>null</code> if a matching assertion could not be found
	*/
	public Assertion fetchByUuid_Last(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Assertion> orderByComparator);

	/**
	* Returns the assertions before and after the current assertion in the ordered set where uuid = &#63;.
	*
	* @param assertionId the primary key of the current assertion
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next assertion
	* @throws NoSuchAssertionException if a assertion with the primary key could not be found
	*/
	public Assertion[] findByUuid_PrevAndNext(long assertionId, String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Assertion> orderByComparator)
		throws NoSuchAssertionException;

	/**
	* Removes all the assertions where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(String uuid);

	/**
	* Returns the number of assertions where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching assertions
	*/
	public int countByUuid(String uuid);

	/**
	* Returns the assertion where generatedKey = &#63; or throws a {@link NoSuchAssertionException} if it could not be found.
	*
	* @param generatedKey the generated key
	* @return the matching assertion
	* @throws NoSuchAssertionException if a matching assertion could not be found
	*/
	public Assertion findByGeneratedKey(String generatedKey)
		throws NoSuchAssertionException;

	/**
	* Returns the assertion where generatedKey = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param generatedKey the generated key
	* @return the matching assertion, or <code>null</code> if a matching assertion could not be found
	*/
	public Assertion fetchByGeneratedKey(String generatedKey);

	/**
	* Returns the assertion where generatedKey = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param generatedKey the generated key
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching assertion, or <code>null</code> if a matching assertion could not be found
	*/
	public Assertion fetchByGeneratedKey(String generatedKey,
		boolean retrieveFromCache);

	/**
	* Removes the assertion where generatedKey = &#63; from the database.
	*
	* @param generatedKey the generated key
	* @return the assertion that was removed
	*/
	public Assertion removeByGeneratedKey(String generatedKey)
		throws NoSuchAssertionException;

	/**
	* Returns the number of assertions where generatedKey = &#63;.
	*
	* @param generatedKey the generated key
	* @return the number of matching assertions
	*/
	public int countByGeneratedKey(String generatedKey);

	/**
	* Caches the assertion in the entity cache if it is enabled.
	*
	* @param assertion the assertion
	*/
	public void cacheResult(Assertion assertion);

	/**
	* Caches the assertions in the entity cache if it is enabled.
	*
	* @param assertions the assertions
	*/
	public void cacheResult(java.util.List<Assertion> assertions);

	/**
	* Creates a new assertion with the primary key. Does not add the assertion to the database.
	*
	* @param assertionId the primary key for the new assertion
	* @return the new assertion
	*/
	public Assertion create(long assertionId);

	/**
	* Removes the assertion with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param assertionId the primary key of the assertion
	* @return the assertion that was removed
	* @throws NoSuchAssertionException if a assertion with the primary key could not be found
	*/
	public Assertion remove(long assertionId) throws NoSuchAssertionException;

	public Assertion updateImpl(Assertion assertion);

	/**
	* Returns the assertion with the primary key or throws a {@link NoSuchAssertionException} if it could not be found.
	*
	* @param assertionId the primary key of the assertion
	* @return the assertion
	* @throws NoSuchAssertionException if a assertion with the primary key could not be found
	*/
	public Assertion findByPrimaryKey(long assertionId)
		throws NoSuchAssertionException;

	/**
	* Returns the assertion with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param assertionId the primary key of the assertion
	* @return the assertion, or <code>null</code> if a assertion with the primary key could not be found
	*/
	public Assertion fetchByPrimaryKey(long assertionId);

	@Override
	public java.util.Map<java.io.Serializable, Assertion> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the assertions.
	*
	* @return the assertions
	*/
	public java.util.List<Assertion> findAll();

	/**
	* Returns a range of all the assertions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of assertions
	* @param end the upper bound of the range of assertions (not inclusive)
	* @return the range of assertions
	*/
	public java.util.List<Assertion> findAll(int start, int end);

	/**
	* Returns an ordered range of all the assertions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of assertions
	* @param end the upper bound of the range of assertions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of assertions
	*/
	public java.util.List<Assertion> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Assertion> orderByComparator);

	/**
	* Returns an ordered range of all the assertions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of assertions
	* @param end the upper bound of the range of assertions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of assertions
	*/
	public java.util.List<Assertion> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Assertion> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the assertions from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of assertions.
	*
	* @return the number of assertions
	*/
	public int countAll();

	@Override
	public java.util.Set<String> getBadColumnNames();
}