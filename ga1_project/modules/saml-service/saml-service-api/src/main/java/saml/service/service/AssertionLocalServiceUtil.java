/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package saml.service.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Assertion. This utility wraps
 * {@link saml.service.service.impl.AssertionLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see AssertionLocalService
 * @see saml.service.service.base.AssertionLocalServiceBaseImpl
 * @see saml.service.service.impl.AssertionLocalServiceImpl
 * @generated
 */
@ProviderType
public class AssertionLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link saml.service.service.impl.AssertionLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the assertion to the database. Also notifies the appropriate model listeners.
	*
	* @param assertion the assertion
	* @return the assertion that was added
	*/
	public static saml.service.model.Assertion addAssertion(
		saml.service.model.Assertion assertion) {
		return getService().addAssertion(assertion);
	}

	public static void cleanDeprecatedAssertion(java.util.Date now)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().cleanDeprecatedAssertion(now);
	}

	/**
	* Creates a new assertion with the primary key. Does not add the assertion to the database.
	*
	* @param assertionId the primary key for the new assertion
	* @return the new assertion
	*/
	public static saml.service.model.Assertion createAssertion(long assertionId) {
		return getService().createAssertion(assertionId);
	}

	/**
	* Deletes the assertion from the database. Also notifies the appropriate model listeners.
	*
	* @param assertion the assertion
	* @return the assertion that was removed
	*/
	public static saml.service.model.Assertion deleteAssertion(
		saml.service.model.Assertion assertion) {
		return getService().deleteAssertion(assertion);
	}

	/**
	* Deletes the assertion with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param assertionId the primary key of the assertion
	* @return the assertion that was removed
	* @throws PortalException if a assertion with the primary key could not be found
	*/
	public static saml.service.model.Assertion deleteAssertion(long assertionId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteAssertion(assertionId);
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link saml.service.model.impl.AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link saml.service.model.impl.AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static saml.service.model.Assertion fetchAssertion(long assertionId) {
		return getService().fetchAssertion(assertionId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	/**
	* Returns the assertion with the primary key.
	*
	* @param assertionId the primary key of the assertion
	* @return the assertion
	* @throws PortalException if a assertion with the primary key could not be found
	*/
	public static saml.service.model.Assertion getAssertion(long assertionId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getAssertion(assertionId);
	}

	/**
	* Returns a range of all the assertions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link saml.service.model.impl.AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of assertions
	* @param end the upper bound of the range of assertions (not inclusive)
	* @return the range of assertions
	*/
	public static java.util.List<saml.service.model.Assertion> getAssertions(
		int start, int end) {
		return getService().getAssertions(start, end);
	}

	/**
	* Returns the number of assertions.
	*
	* @return the number of assertions
	*/
	public static int getAssertionsCount() {
		return getService().getAssertionsCount();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	public static void printMsg2(String message) {
		getService().printMsg2(message);
	}

	public static boolean replayedAssertion(String generatedKey,
		java.util.Date notOnOrAfter)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().replayedAssertion(generatedKey, notOnOrAfter);
	}

	/**
	* Updates the assertion in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param assertion the assertion
	* @return the assertion that was updated
	*/
	public static saml.service.model.Assertion updateAssertion(
		saml.service.model.Assertion assertion) {
		return getService().updateAssertion(assertion);
	}

	public static AssertionLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<AssertionLocalService, AssertionLocalService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(AssertionLocalService.class);

		ServiceTracker<AssertionLocalService, AssertionLocalService> serviceTracker =
			new ServiceTracker<AssertionLocalService, AssertionLocalService>(bundle.getBundleContext(),
				AssertionLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}