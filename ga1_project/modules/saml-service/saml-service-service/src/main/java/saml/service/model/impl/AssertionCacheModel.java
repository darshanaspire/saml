/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package saml.service.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import saml.service.model.Assertion;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Assertion in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Assertion
 * @generated
 */
@ProviderType
public class AssertionCacheModel implements CacheModel<Assertion>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AssertionCacheModel)) {
			return false;
		}

		AssertionCacheModel assertionCacheModel = (AssertionCacheModel)obj;

		if (assertionId == assertionCacheModel.assertionId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, assertionId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", assertionId=");
		sb.append(assertionId);
		sb.append(", generatedKey=");
		sb.append(generatedKey);
		sb.append(", notOnOrAfter=");
		sb.append(notOnOrAfter);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Assertion toEntityModel() {
		AssertionImpl assertionImpl = new AssertionImpl();

		if (uuid == null) {
			assertionImpl.setUuid("");
		}
		else {
			assertionImpl.setUuid(uuid);
		}

		assertionImpl.setAssertionId(assertionId);

		if (generatedKey == null) {
			assertionImpl.setGeneratedKey("");
		}
		else {
			assertionImpl.setGeneratedKey(generatedKey);
		}

		if (notOnOrAfter == Long.MIN_VALUE) {
			assertionImpl.setNotOnOrAfter(null);
		}
		else {
			assertionImpl.setNotOnOrAfter(new Date(notOnOrAfter));
		}

		assertionImpl.resetOriginalValues();

		return assertionImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		assertionId = objectInput.readLong();
		generatedKey = objectInput.readUTF();
		notOnOrAfter = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(assertionId);

		if (generatedKey == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(generatedKey);
		}

		objectOutput.writeLong(notOnOrAfter);
	}

	public String uuid;
	public long assertionId;
	public String generatedKey;
	public long notOnOrAfter;
}