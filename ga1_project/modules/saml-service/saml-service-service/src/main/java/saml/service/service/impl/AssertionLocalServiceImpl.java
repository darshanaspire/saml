/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 */

package saml.service.service.impl;

import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.UserPersistence;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.CalendarUtil;
import com.liferay.portal.kernel.util.Validator;

import saml.service.exception.NoSuchAssertionException;
import saml.service.model.Assertion;
import saml.service.service.base.AssertionLocalServiceBaseImpl;

/**
 * The implementation of the assertion local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun
 * ServiceBuilder to copy their definitions into the
 * {@link saml.service.service.AssertionLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the
 * propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AssertionLocalServiceBaseImpl
 * @see saml.service.service.AssertionLocalServiceUtil
 */
public class AssertionLocalServiceImpl extends AssertionLocalServiceBaseImpl {
  /*
   * NOTE FOR DEVELOPERS:
   *
   * Never reference this class directly. Always use {@link
   * saml.service.service.AssertionLocalServiceUtil} to access the assertion local service.
   */
  private static Log log = LogFactoryUtil.getLog(AssertionLocalServiceImpl.class);
  public static String CLEAN_DEPRECATED_ASSERTION =
      "DELETE FROM Assertion WHERE (Assertion.notOnOrAfter <?) ";

  public void cleanDeprecatedAssertion(Date now) throws SystemException {
    System.out.println("This is Clean Deprecated Assertion");
    Timestamp now_TS = CalendarUtil.getTimestamp(now);
    String query =
        "DELETE FROM Saml_Assertion WHERE (Saml_Assertion.notOnOrAfter < '" + now_TS + "' ) ";
    executeCustomQuery(query, null, userPersistence);
  }

  public static void executeCustomQuery(String queryString, List<String> queryParamList,
      UserPersistence userPersistence) throws SystemException {
    Session session = null;
    try {
      session = userPersistence.openSession();
      SQLQuery query = session.createSQLQuery(queryString);
      query.setCacheable(false);
      QueryPos qPos = QueryPos.getInstance(query);
      // add the dynamic parameter value in the query
      if (Validator.isNotNull(queryParamList) && queryParamList.size() > 0) {
        for (String queryParam : queryParamList) {
          qPos.add(queryParam);
        }
      }
      System.out.println("-----------------------------------------------------------");
      System.out.println(query);
      System.out.println("-----------------------------------------------------------");
      query.executeUpdate();
    } catch (Exception e) {
      throw new SystemException(e);
    } finally {
      userPersistence.closeSession(session);
    }
  }

  public boolean replayedAssertion(String generatedKey, Date notOnOrAfter) throws SystemException {
    log.info("We are in replayed Assersion....");
    try {
      assertionPersistence.findByGeneratedKey(generatedKey);
      return true;
    } catch (NoSuchAssertionException nsae) {
      long assertionId = counterLocalService.increment();

      Assertion assertion = assertionPersistence.create(assertionId);

      assertion.setGeneratedKey(generatedKey);
      assertion.setNotOnOrAfter(notOnOrAfter);

      assertionLocalService.updateAssertion(assertion);

      return false;
    }
  }

  public void printMsg2(String message) {
    log.info(message);
  }


}
