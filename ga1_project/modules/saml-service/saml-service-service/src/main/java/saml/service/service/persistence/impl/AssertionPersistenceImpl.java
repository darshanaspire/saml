/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package saml.service.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import saml.service.exception.NoSuchAssertionException;

import saml.service.model.Assertion;
import saml.service.model.impl.AssertionImpl;
import saml.service.model.impl.AssertionModelImpl;

import saml.service.service.persistence.AssertionPersistence;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the assertion service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AssertionPersistence
 * @see saml.service.service.persistence.AssertionUtil
 * @generated
 */
@ProviderType
public class AssertionPersistenceImpl extends BasePersistenceImpl<Assertion>
	implements AssertionPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link AssertionUtil} to access the assertion persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = AssertionImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AssertionModelImpl.ENTITY_CACHE_ENABLED,
			AssertionModelImpl.FINDER_CACHE_ENABLED, AssertionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AssertionModelImpl.ENTITY_CACHE_ENABLED,
			AssertionModelImpl.FINDER_CACHE_ENABLED, AssertionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AssertionModelImpl.ENTITY_CACHE_ENABLED,
			AssertionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(AssertionModelImpl.ENTITY_CACHE_ENABLED,
			AssertionModelImpl.FINDER_CACHE_ENABLED, AssertionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(AssertionModelImpl.ENTITY_CACHE_ENABLED,
			AssertionModelImpl.FINDER_CACHE_ENABLED, AssertionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			AssertionModelImpl.UUID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(AssertionModelImpl.ENTITY_CACHE_ENABLED,
			AssertionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the assertions where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching assertions
	 */
	@Override
	public List<Assertion> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the assertions where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of assertions
	 * @param end the upper bound of the range of assertions (not inclusive)
	 * @return the range of matching assertions
	 */
	@Override
	public List<Assertion> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the assertions where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of assertions
	 * @param end the upper bound of the range of assertions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching assertions
	 */
	@Override
	public List<Assertion> findByUuid(String uuid, int start, int end,
		OrderByComparator<Assertion> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the assertions where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of assertions
	 * @param end the upper bound of the range of assertions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching assertions
	 */
	@Override
	public List<Assertion> findByUuid(String uuid, int start, int end,
		OrderByComparator<Assertion> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<Assertion> list = null;

		if (retrieveFromCache) {
			list = (List<Assertion>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Assertion assertion : list) {
					if (!Objects.equals(uuid, assertion.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ASSERTION_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(AssertionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<Assertion>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Assertion>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first assertion in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assertion
	 * @throws NoSuchAssertionException if a matching assertion could not be found
	 */
	@Override
	public Assertion findByUuid_First(String uuid,
		OrderByComparator<Assertion> orderByComparator)
		throws NoSuchAssertionException {
		Assertion assertion = fetchByUuid_First(uuid, orderByComparator);

		if (assertion != null) {
			return assertion;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchAssertionException(msg.toString());
	}

	/**
	 * Returns the first assertion in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching assertion, or <code>null</code> if a matching assertion could not be found
	 */
	@Override
	public Assertion fetchByUuid_First(String uuid,
		OrderByComparator<Assertion> orderByComparator) {
		List<Assertion> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last assertion in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assertion
	 * @throws NoSuchAssertionException if a matching assertion could not be found
	 */
	@Override
	public Assertion findByUuid_Last(String uuid,
		OrderByComparator<Assertion> orderByComparator)
		throws NoSuchAssertionException {
		Assertion assertion = fetchByUuid_Last(uuid, orderByComparator);

		if (assertion != null) {
			return assertion;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchAssertionException(msg.toString());
	}

	/**
	 * Returns the last assertion in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching assertion, or <code>null</code> if a matching assertion could not be found
	 */
	@Override
	public Assertion fetchByUuid_Last(String uuid,
		OrderByComparator<Assertion> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<Assertion> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the assertions before and after the current assertion in the ordered set where uuid = &#63;.
	 *
	 * @param assertionId the primary key of the current assertion
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next assertion
	 * @throws NoSuchAssertionException if a assertion with the primary key could not be found
	 */
	@Override
	public Assertion[] findByUuid_PrevAndNext(long assertionId, String uuid,
		OrderByComparator<Assertion> orderByComparator)
		throws NoSuchAssertionException {
		Assertion assertion = findByPrimaryKey(assertionId);

		Session session = null;

		try {
			session = openSession();

			Assertion[] array = new AssertionImpl[3];

			array[0] = getByUuid_PrevAndNext(session, assertion, uuid,
					orderByComparator, true);

			array[1] = assertion;

			array[2] = getByUuid_PrevAndNext(session, assertion, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Assertion getByUuid_PrevAndNext(Session session,
		Assertion assertion, String uuid,
		OrderByComparator<Assertion> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ASSERTION_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals("")) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(AssertionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(assertion);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Assertion> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the assertions where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (Assertion assertion : findByUuid(uuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(assertion);
		}
	}

	/**
	 * Returns the number of assertions where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching assertions
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ASSERTION_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "assertion.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "assertion.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(assertion.uuid IS NULL OR assertion.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_GENERATEDKEY = new FinderPath(AssertionModelImpl.ENTITY_CACHE_ENABLED,
			AssertionModelImpl.FINDER_CACHE_ENABLED, AssertionImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByGeneratedKey",
			new String[] { String.class.getName() },
			AssertionModelImpl.GENERATEDKEY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_GENERATEDKEY = new FinderPath(AssertionModelImpl.ENTITY_CACHE_ENABLED,
			AssertionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGeneratedKey",
			new String[] { String.class.getName() });

	/**
	 * Returns the assertion where generatedKey = &#63; or throws a {@link NoSuchAssertionException} if it could not be found.
	 *
	 * @param generatedKey the generated key
	 * @return the matching assertion
	 * @throws NoSuchAssertionException if a matching assertion could not be found
	 */
	@Override
	public Assertion findByGeneratedKey(String generatedKey)
		throws NoSuchAssertionException {
		Assertion assertion = fetchByGeneratedKey(generatedKey);

		if (assertion == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("generatedKey=");
			msg.append(generatedKey);

			msg.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchAssertionException(msg.toString());
		}

		return assertion;
	}

	/**
	 * Returns the assertion where generatedKey = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param generatedKey the generated key
	 * @return the matching assertion, or <code>null</code> if a matching assertion could not be found
	 */
	@Override
	public Assertion fetchByGeneratedKey(String generatedKey) {
		return fetchByGeneratedKey(generatedKey, true);
	}

	/**
	 * Returns the assertion where generatedKey = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param generatedKey the generated key
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching assertion, or <code>null</code> if a matching assertion could not be found
	 */
	@Override
	public Assertion fetchByGeneratedKey(String generatedKey,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { generatedKey };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_GENERATEDKEY,
					finderArgs, this);
		}

		if (result instanceof Assertion) {
			Assertion assertion = (Assertion)result;

			if (!Objects.equals(generatedKey, assertion.getGeneratedKey())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_ASSERTION_WHERE);

			boolean bindGeneratedKey = false;

			if (generatedKey == null) {
				query.append(_FINDER_COLUMN_GENERATEDKEY_GENERATEDKEY_1);
			}
			else if (generatedKey.equals("")) {
				query.append(_FINDER_COLUMN_GENERATEDKEY_GENERATEDKEY_3);
			}
			else {
				bindGeneratedKey = true;

				query.append(_FINDER_COLUMN_GENERATEDKEY_GENERATEDKEY_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindGeneratedKey) {
					qPos.add(generatedKey);
				}

				List<Assertion> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_GENERATEDKEY,
						finderArgs, list);
				}
				else {
					Assertion assertion = list.get(0);

					result = assertion;

					cacheResult(assertion);
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_GENERATEDKEY,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Assertion)result;
		}
	}

	/**
	 * Removes the assertion where generatedKey = &#63; from the database.
	 *
	 * @param generatedKey the generated key
	 * @return the assertion that was removed
	 */
	@Override
	public Assertion removeByGeneratedKey(String generatedKey)
		throws NoSuchAssertionException {
		Assertion assertion = findByGeneratedKey(generatedKey);

		return remove(assertion);
	}

	/**
	 * Returns the number of assertions where generatedKey = &#63;.
	 *
	 * @param generatedKey the generated key
	 * @return the number of matching assertions
	 */
	@Override
	public int countByGeneratedKey(String generatedKey) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_GENERATEDKEY;

		Object[] finderArgs = new Object[] { generatedKey };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ASSERTION_WHERE);

			boolean bindGeneratedKey = false;

			if (generatedKey == null) {
				query.append(_FINDER_COLUMN_GENERATEDKEY_GENERATEDKEY_1);
			}
			else if (generatedKey.equals("")) {
				query.append(_FINDER_COLUMN_GENERATEDKEY_GENERATEDKEY_3);
			}
			else {
				bindGeneratedKey = true;

				query.append(_FINDER_COLUMN_GENERATEDKEY_GENERATEDKEY_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindGeneratedKey) {
					qPos.add(generatedKey);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GENERATEDKEY_GENERATEDKEY_1 = "assertion.generatedKey IS NULL";
	private static final String _FINDER_COLUMN_GENERATEDKEY_GENERATEDKEY_2 = "assertion.generatedKey = ?";
	private static final String _FINDER_COLUMN_GENERATEDKEY_GENERATEDKEY_3 = "(assertion.generatedKey IS NULL OR assertion.generatedKey = '')";

	public AssertionPersistenceImpl() {
		setModelClass(Assertion.class);

		try {
			Field field = BasePersistenceImpl.class.getDeclaredField(
					"_dbColumnNames");

			field.setAccessible(true);

			Map<String, String> dbColumnNames = new HashMap<String, String>();

			dbColumnNames.put("uuid", "uuid_");

			field.set(this, dbColumnNames);
		}
		catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e, e);
			}
		}
	}

	/**
	 * Caches the assertion in the entity cache if it is enabled.
	 *
	 * @param assertion the assertion
	 */
	@Override
	public void cacheResult(Assertion assertion) {
		entityCache.putResult(AssertionModelImpl.ENTITY_CACHE_ENABLED,
			AssertionImpl.class, assertion.getPrimaryKey(), assertion);

		finderCache.putResult(FINDER_PATH_FETCH_BY_GENERATEDKEY,
			new Object[] { assertion.getGeneratedKey() }, assertion);

		assertion.resetOriginalValues();
	}

	/**
	 * Caches the assertions in the entity cache if it is enabled.
	 *
	 * @param assertions the assertions
	 */
	@Override
	public void cacheResult(List<Assertion> assertions) {
		for (Assertion assertion : assertions) {
			if (entityCache.getResult(AssertionModelImpl.ENTITY_CACHE_ENABLED,
						AssertionImpl.class, assertion.getPrimaryKey()) == null) {
				cacheResult(assertion);
			}
			else {
				assertion.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all assertions.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(AssertionImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the assertion.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Assertion assertion) {
		entityCache.removeResult(AssertionModelImpl.ENTITY_CACHE_ENABLED,
			AssertionImpl.class, assertion.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((AssertionModelImpl)assertion, true);
	}

	@Override
	public void clearCache(List<Assertion> assertions) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Assertion assertion : assertions) {
			entityCache.removeResult(AssertionModelImpl.ENTITY_CACHE_ENABLED,
				AssertionImpl.class, assertion.getPrimaryKey());

			clearUniqueFindersCache((AssertionModelImpl)assertion, true);
		}
	}

	protected void cacheUniqueFindersCache(
		AssertionModelImpl assertionModelImpl) {
		Object[] args = new Object[] { assertionModelImpl.getGeneratedKey() };

		finderCache.putResult(FINDER_PATH_COUNT_BY_GENERATEDKEY, args,
			Long.valueOf(1), false);
		finderCache.putResult(FINDER_PATH_FETCH_BY_GENERATEDKEY, args,
			assertionModelImpl, false);
	}

	protected void clearUniqueFindersCache(
		AssertionModelImpl assertionModelImpl, boolean clearCurrent) {
		if (clearCurrent) {
			Object[] args = new Object[] { assertionModelImpl.getGeneratedKey() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_GENERATEDKEY, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_GENERATEDKEY, args);
		}

		if ((assertionModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_GENERATEDKEY.getColumnBitmask()) != 0) {
			Object[] args = new Object[] {
					assertionModelImpl.getOriginalGeneratedKey()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_GENERATEDKEY, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_GENERATEDKEY, args);
		}
	}

	/**
	 * Creates a new assertion with the primary key. Does not add the assertion to the database.
	 *
	 * @param assertionId the primary key for the new assertion
	 * @return the new assertion
	 */
	@Override
	public Assertion create(long assertionId) {
		Assertion assertion = new AssertionImpl();

		assertion.setNew(true);
		assertion.setPrimaryKey(assertionId);

		String uuid = PortalUUIDUtil.generate();

		assertion.setUuid(uuid);

		return assertion;
	}

	/**
	 * Removes the assertion with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param assertionId the primary key of the assertion
	 * @return the assertion that was removed
	 * @throws NoSuchAssertionException if a assertion with the primary key could not be found
	 */
	@Override
	public Assertion remove(long assertionId) throws NoSuchAssertionException {
		return remove((Serializable)assertionId);
	}

	/**
	 * Removes the assertion with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the assertion
	 * @return the assertion that was removed
	 * @throws NoSuchAssertionException if a assertion with the primary key could not be found
	 */
	@Override
	public Assertion remove(Serializable primaryKey)
		throws NoSuchAssertionException {
		Session session = null;

		try {
			session = openSession();

			Assertion assertion = (Assertion)session.get(AssertionImpl.class,
					primaryKey);

			if (assertion == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAssertionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(assertion);
		}
		catch (NoSuchAssertionException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Assertion removeImpl(Assertion assertion) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(assertion)) {
				assertion = (Assertion)session.get(AssertionImpl.class,
						assertion.getPrimaryKeyObj());
			}

			if (assertion != null) {
				session.delete(assertion);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (assertion != null) {
			clearCache(assertion);
		}

		return assertion;
	}

	@Override
	public Assertion updateImpl(Assertion assertion) {
		boolean isNew = assertion.isNew();

		if (!(assertion instanceof AssertionModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(assertion.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(assertion);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in assertion proxy " +
					invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom Assertion implementation " +
				assertion.getClass());
		}

		AssertionModelImpl assertionModelImpl = (AssertionModelImpl)assertion;

		if (Validator.isNull(assertion.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			assertion.setUuid(uuid);
		}

		Session session = null;

		try {
			session = openSession();

			if (assertion.isNew()) {
				session.save(assertion);

				assertion.setNew(false);
			}
			else {
				assertion = (Assertion)session.merge(assertion);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!AssertionModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] { assertionModelImpl.getUuid() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((assertionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						assertionModelImpl.getOriginalUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { assertionModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}
		}

		entityCache.putResult(AssertionModelImpl.ENTITY_CACHE_ENABLED,
			AssertionImpl.class, assertion.getPrimaryKey(), assertion, false);

		clearUniqueFindersCache(assertionModelImpl, false);
		cacheUniqueFindersCache(assertionModelImpl);

		assertion.resetOriginalValues();

		return assertion;
	}

	/**
	 * Returns the assertion with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the assertion
	 * @return the assertion
	 * @throws NoSuchAssertionException if a assertion with the primary key could not be found
	 */
	@Override
	public Assertion findByPrimaryKey(Serializable primaryKey)
		throws NoSuchAssertionException {
		Assertion assertion = fetchByPrimaryKey(primaryKey);

		if (assertion == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchAssertionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return assertion;
	}

	/**
	 * Returns the assertion with the primary key or throws a {@link NoSuchAssertionException} if it could not be found.
	 *
	 * @param assertionId the primary key of the assertion
	 * @return the assertion
	 * @throws NoSuchAssertionException if a assertion with the primary key could not be found
	 */
	@Override
	public Assertion findByPrimaryKey(long assertionId)
		throws NoSuchAssertionException {
		return findByPrimaryKey((Serializable)assertionId);
	}

	/**
	 * Returns the assertion with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the assertion
	 * @return the assertion, or <code>null</code> if a assertion with the primary key could not be found
	 */
	@Override
	public Assertion fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(AssertionModelImpl.ENTITY_CACHE_ENABLED,
				AssertionImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Assertion assertion = (Assertion)serializable;

		if (assertion == null) {
			Session session = null;

			try {
				session = openSession();

				assertion = (Assertion)session.get(AssertionImpl.class,
						primaryKey);

				if (assertion != null) {
					cacheResult(assertion);
				}
				else {
					entityCache.putResult(AssertionModelImpl.ENTITY_CACHE_ENABLED,
						AssertionImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(AssertionModelImpl.ENTITY_CACHE_ENABLED,
					AssertionImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return assertion;
	}

	/**
	 * Returns the assertion with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param assertionId the primary key of the assertion
	 * @return the assertion, or <code>null</code> if a assertion with the primary key could not be found
	 */
	@Override
	public Assertion fetchByPrimaryKey(long assertionId) {
		return fetchByPrimaryKey((Serializable)assertionId);
	}

	@Override
	public Map<Serializable, Assertion> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Assertion> map = new HashMap<Serializable, Assertion>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Assertion assertion = fetchByPrimaryKey(primaryKey);

			if (assertion != null) {
				map.put(primaryKey, assertion);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(AssertionModelImpl.ENTITY_CACHE_ENABLED,
					AssertionImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Assertion)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_ASSERTION_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(",");
		}

		query.setIndex(query.index() - 1);

		query.append(")");

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Assertion assertion : (List<Assertion>)q.list()) {
				map.put(assertion.getPrimaryKeyObj(), assertion);

				cacheResult(assertion);

				uncachedPrimaryKeys.remove(assertion.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(AssertionModelImpl.ENTITY_CACHE_ENABLED,
					AssertionImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the assertions.
	 *
	 * @return the assertions
	 */
	@Override
	public List<Assertion> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the assertions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of assertions
	 * @param end the upper bound of the range of assertions (not inclusive)
	 * @return the range of assertions
	 */
	@Override
	public List<Assertion> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the assertions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of assertions
	 * @param end the upper bound of the range of assertions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of assertions
	 */
	@Override
	public List<Assertion> findAll(int start, int end,
		OrderByComparator<Assertion> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the assertions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link AssertionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of assertions
	 * @param end the upper bound of the range of assertions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of assertions
	 */
	@Override
	public List<Assertion> findAll(int start, int end,
		OrderByComparator<Assertion> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Assertion> list = null;

		if (retrieveFromCache) {
			list = (List<Assertion>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_ASSERTION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ASSERTION;

				if (pagination) {
					sql = sql.concat(AssertionModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Assertion>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Assertion>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the assertions from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Assertion assertion : findAll()) {
			remove(assertion);
		}
	}

	/**
	 * Returns the number of assertions.
	 *
	 * @return the number of assertions
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ASSERTION);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return AssertionModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the assertion persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(AssertionImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_ASSERTION = "SELECT assertion FROM Assertion assertion";
	private static final String _SQL_SELECT_ASSERTION_WHERE_PKS_IN = "SELECT assertion FROM Assertion assertion WHERE assertionId IN (";
	private static final String _SQL_SELECT_ASSERTION_WHERE = "SELECT assertion FROM Assertion assertion WHERE ";
	private static final String _SQL_COUNT_ASSERTION = "SELECT COUNT(assertion) FROM Assertion assertion";
	private static final String _SQL_COUNT_ASSERTION_WHERE = "SELECT COUNT(assertion) FROM Assertion assertion WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "assertion.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Assertion exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Assertion exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(AssertionPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}