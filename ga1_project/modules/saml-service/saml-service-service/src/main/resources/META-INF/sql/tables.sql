create table Saml_Assertion (
	uuid_ VARCHAR(75) null,
	assertionId LONG not null primary key,
	generatedKey VARCHAR(75) null,
	notOnOrAfter DATE null
);