/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package saml.service.service.persistence.test;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.ReflectionTestUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;
import com.liferay.portal.test.rule.TransactionalTestRule;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import saml.service.exception.NoSuchAssertionException;

import saml.service.model.Assertion;

import saml.service.service.AssertionLocalServiceUtil;
import saml.service.service.persistence.AssertionPersistence;
import saml.service.service.persistence.AssertionUtil;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class AssertionPersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED,
				"saml.service.service"));

	@Before
	public void setUp() {
		_persistence = AssertionUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<Assertion> iterator = _assertions.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Assertion assertion = _persistence.create(pk);

		Assert.assertNotNull(assertion);

		Assert.assertEquals(assertion.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		Assertion newAssertion = addAssertion();

		_persistence.remove(newAssertion);

		Assertion existingAssertion = _persistence.fetchByPrimaryKey(newAssertion.getPrimaryKey());

		Assert.assertNull(existingAssertion);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addAssertion();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Assertion newAssertion = _persistence.create(pk);

		newAssertion.setUuid(RandomTestUtil.randomString());

		newAssertion.setGeneratedKey(RandomTestUtil.randomString());

		newAssertion.setNotOnOrAfter(RandomTestUtil.nextDate());

		_assertions.add(_persistence.update(newAssertion));

		Assertion existingAssertion = _persistence.findByPrimaryKey(newAssertion.getPrimaryKey());

		Assert.assertEquals(existingAssertion.getUuid(), newAssertion.getUuid());
		Assert.assertEquals(existingAssertion.getAssertionId(),
			newAssertion.getAssertionId());
		Assert.assertEquals(existingAssertion.getGeneratedKey(),
			newAssertion.getGeneratedKey());
		Assert.assertEquals(Time.getShortTimestamp(
				existingAssertion.getNotOnOrAfter()),
			Time.getShortTimestamp(newAssertion.getNotOnOrAfter()));
	}

	@Test
	public void testCountByUuid() throws Exception {
		_persistence.countByUuid("");

		_persistence.countByUuid("null");

		_persistence.countByUuid((String)null);
	}

	@Test
	public void testCountByGeneratedKey() throws Exception {
		_persistence.countByGeneratedKey("");

		_persistence.countByGeneratedKey("null");

		_persistence.countByGeneratedKey((String)null);
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		Assertion newAssertion = addAssertion();

		Assertion existingAssertion = _persistence.findByPrimaryKey(newAssertion.getPrimaryKey());

		Assert.assertEquals(existingAssertion, newAssertion);
	}

	@Test(expected = NoSuchAssertionException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			getOrderByComparator());
	}

	protected OrderByComparator<Assertion> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create("Saml_Assertion", "uuid",
			true, "assertionId", true, "generatedKey", true, "notOnOrAfter",
			true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		Assertion newAssertion = addAssertion();

		Assertion existingAssertion = _persistence.fetchByPrimaryKey(newAssertion.getPrimaryKey());

		Assert.assertEquals(existingAssertion, newAssertion);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Assertion missingAssertion = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingAssertion);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		Assertion newAssertion1 = addAssertion();
		Assertion newAssertion2 = addAssertion();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newAssertion1.getPrimaryKey());
		primaryKeys.add(newAssertion2.getPrimaryKey());

		Map<Serializable, Assertion> assertions = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, assertions.size());
		Assert.assertEquals(newAssertion1,
			assertions.get(newAssertion1.getPrimaryKey()));
		Assert.assertEquals(newAssertion2,
			assertions.get(newAssertion2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		long pk1 = RandomTestUtil.nextLong();

		long pk2 = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, Assertion> assertions = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(assertions.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		Assertion newAssertion = addAssertion();

		long pk = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newAssertion.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, Assertion> assertions = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, assertions.size());
		Assert.assertEquals(newAssertion,
			assertions.get(newAssertion.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, Assertion> assertions = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(assertions.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		Assertion newAssertion = addAssertion();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newAssertion.getPrimaryKey());

		Map<Serializable, Assertion> assertions = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, assertions.size());
		Assert.assertEquals(newAssertion,
			assertions.get(newAssertion.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery = AssertionLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod<Assertion>() {
				@Override
				public void performAction(Assertion assertion) {
					Assert.assertNotNull(assertion);

					count.increment();
				}
			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		Assertion newAssertion = addAssertion();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Assertion.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("assertionId",
				newAssertion.getAssertionId()));

		List<Assertion> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Assertion existingAssertion = result.get(0);

		Assert.assertEquals(existingAssertion, newAssertion);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Assertion.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("assertionId",
				RandomTestUtil.nextLong()));

		List<Assertion> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		Assertion newAssertion = addAssertion();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Assertion.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("assertionId"));

		Object newAssertionId = newAssertion.getAssertionId();

		dynamicQuery.add(RestrictionsFactoryUtil.in("assertionId",
				new Object[] { newAssertionId }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingAssertionId = result.get(0);

		Assert.assertEquals(existingAssertionId, newAssertionId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Assertion.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("assertionId"));

		dynamicQuery.add(RestrictionsFactoryUtil.in("assertionId",
				new Object[] { RandomTestUtil.nextLong() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testResetOriginalValues() throws Exception {
		Assertion newAssertion = addAssertion();

		_persistence.clearCache();

		Assertion existingAssertion = _persistence.findByPrimaryKey(newAssertion.getPrimaryKey());

		Assert.assertTrue(Objects.equals(existingAssertion.getGeneratedKey(),
				ReflectionTestUtil.invoke(existingAssertion,
					"getOriginalGeneratedKey", new Class<?>[0])));
	}

	protected Assertion addAssertion() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Assertion assertion = _persistence.create(pk);

		assertion.setUuid(RandomTestUtil.randomString());

		assertion.setGeneratedKey(RandomTestUtil.randomString());

		assertion.setNotOnOrAfter(RandomTestUtil.nextDate());

		_assertions.add(_persistence.update(assertion));

		return assertion;
	}

	private List<Assertion> _assertions = new ArrayList<Assertion>();
	private AssertionPersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}